/**
 * @author By lizhihu
 * @date By 2016-01-23
 */
function hideList() {
	$(".inputList").slideUp(500);
}

function imgChange() {
	$($(".biaoNow").find("img")[1])
}
var gradeNameModel = [];
var classNameModel = [];
var tableHtml = ""
var PersonHealth = function() {
	this.term = "";
	this.year = "";
	this.grade = "";
	this.classRoom = "";
	this.sex = "";
	$.easing.def = "easeInBack";

}
PersonHealth.prototype.getPersonListYear = function() {

	//学年
	$("#yearId").on("click", function(e) {
		hideList();
		if ($("#yearListId").css("display") != "block") {
			$("#yearListId").slideDown(500);
			$("#yearId img").attr("src", "img/moreup_gray.png");
		} else {
			$("#yearListId").slideUp(500);
			$("#yearId img").attr("src", "img/moredown_gray.png");
		}
        stopBubble(e)
		//event.stopPropagation();
	});
	$("#yearListId .list div").each(function(index, val) {
        $("#choiceYear").html($($("#yearListId .list div")[0]).context.innerText);
        $("#yearListId .list div")
			return function() {
				$($("#yearListId .list div")[index]).click(function() {
					var _this = this;
					$("#choiceYear").html($(_this).context.innerText);
					$("#yearListId").slideUp(500);
				})
			}(index)

		})
		//点击现在
	$("#nowYear").click(function() {
		$("#choiceYear").html($($("#yearListId .list div")[0]).context.innerText);
		$("#yearListId").slideUp(500);
		$("#yearId img").attr("src", "img/moredown_gray.png");
	})
	$(document).click(function(event) {
		$("#yearListId").slideUp(500);
		$("#yearId img").attr("src", "img/moredown_gray.png");
	})
};
PersonHealth.prototype.getPersonListTerm = function() {
	$("#termId").on("click", function(e) {
			hideList();
			if ($("#termListId").css("display") != "block") {
				$("#termListId").slideDown(500);
				$("#termId img").attr("src", "img/moreup_gray.png");
			} else {
				$("#termListId").slideUp(500);
				$("#termId img").attr("src", "img/moredown_gray.png");
			}
        stopBubble(e)
			//event.stopPropagation();
		})
		//选择学期
	$("#termListId .list div").each(function(index, val) {
        $("#choiceTerm").html($($("#termListId .list div")[0]).context.innerText);
		return function() {
			$($("#termListId .list div")[index]).click(function() {
				var _this = this;
				$("#choiceTerm").html($(_this).context.innerText);

				$("#termListId").slideUp(500);
				$("#termId img").attr("src", "img/moredown_gray.png");
			})
		}(index)

	})
	$(document).click(function(event) {
		$("#termListId").slideUp(500);
		$("#termId img").attr("src", "img/moredown_gray.png");
	})
}
PersonHealth.prototype.getPersonListGrade = function() {
    $("#choiceGrade").html($($("#gradeListId .list div")[0]).context.innerText);
		$("#gradeId").on("click", function(e) {
				hideList();
				if ($("#gradeListId").css("display") != "block") {
					$("#gradeListId").slideDown(500);
					$("#gradeId img").attr("src", "img/moreup.png");
				} else {
					$("#gradeListId").slideUp(500);
					$("#gradeId img").attr("src", "img/moredown.png");
				}
            stopBubble(e)
				//event.stopPropagation();
			})
			//选择学期
		$("#gradeListId .list div").each(function(index, val) {
			return function() {
				$($("#gradeListId .list div")[index]).click(function() {
					var _this = this;
					$("#choiceGrade").html($(_this).context.innerText);
					$("#gradeListId").slideUp(500);
					$("#gradeId img").attr("src", "img/moredown.png");

                    for (var i = 0; i < gradeNameModel.length; i++) {
                        if ($("#choiceGrade").text() == gradeNameModel[i]) {
                            classIdNum = i;
                        }
                    }
                    var classHtml = "";
                    for (var j = 0; j < classNameModel[classIdNum].length; j++) {
                        classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
                    }
                    $("#classListHtmlId").html(classHtml);
                    $("#choiceClass").html($($("#classListHtmlId div")[0]).text());
				})
			}(index)

		})
		$(document).click(function(event) {
			$("#gradeListId").slideUp(500);
			$("#gradeId img").attr("src", "img/moredown.png");
		})
	}
	//班级
PersonHealth.prototype.getPersonListClass = function() {
		$("#classId").on("click", function(e) {
					hideList();
					if ($("#classListId").css("display") != "block") {
						$("#classListId").slideDown(500);
						$("#classId img").attr("src", "img/moreup_gray.png");
					} else {
						$("#classListId").slideUp(500);
						$("#classId img").attr("src", "img/moredown_gray.png");
					}
            stopBubble(e)
					//event.stopPropagation();
            $("#classListId .list div").each(function(index, val) {
                return function() {
                    $($("#classListId .list div")[index]).click(function() {
                        var _this = this;
                        $("#choiceClass").html($(_this).context.innerText);
                        console.log($(_this).context.innerText)
                        $("#classListId").slideUp(500);
                        $("#classId img").attr("src", "img/moredown_gray.png");
                    })
                }(index)

            })

        })

				$(document).click(function(event) {
					$("#classListId").slideUp(500);
					$("#classId img").attr("src", "img/moredown_gray.png");
				})

			//选择学期

	}
	//健康评分表
PersonHealth.prototype.bindEvent = function() {
	var claerTime, claerTime1;
	$(".biaoNow").hover(function() {
		clearTimeout(claerTime1);
		clearTimeout(claerTime);
		if ($(".biaoList").css("display") != "block") {
			$(".biaoList").slideDown('500');
			//小三角的替换
			$($(".biaoNow").find("img")[0]).attr("src", "img/moreup_gray.png");
		}
	}, function(event) {
		claerTime = setTimeout(function() {
			$(".biaoList").slideUp('500');
			$($(".biaoNow").find("img")[0]).attr("src", "img/moredown_gray.png");
		}, 500)

	});
	$(".biaoList").hover(function() {
		clearTimeout(claerTime);
		clearTimeout(claerTime1);
		$(".biaoList").css("display", "block");
	}, function() {
		claerTime1 = setTimeout(function() {
			$(".biaoList").slideUp('500');
			$($(".biaoNow").find("img")[0]).attr("src", "img/moredown_gray.png");
		}, 500)
	})

}
PersonHealth.prototype.getDefault = function(){
    tableHtml=""
    $("#hasData  tr:not(:first)").empty("");
    //$("table tr").empty();
    this.year = $("#choiceYear").text();
    this.term = $("#choiceTerm").text();
    this.grade = $("#choiceGrade").text();
    this.classRoom = $("#choiceClass").text();
    this.sex = $('#checkSex input[type="radio"]:checked ').val();
    var sexId = ""
    var classId = this.grade + "," + this.classRoom;
    classId = dataArr[classId];
    console.log(classId)
    if (this.sex == 3) {
        sexId = ""
    } else {
        sexId = this.sex;

    }
        $("#title").html(this.year + "学年" + this.term + this.grade + this.classRoom + "体质健康评分表")
        var url = getURL();
        var classAndGrade = this.grade+","+this.classRoom;
        classAndGrade = dataBrr[classAndGrade];
        var classTerm = this.term;
        classTerm = dataTerm[classTerm];
        //2016 1 2班有数据 全部
        var data = {
            "sex": sexId,
            "class_id": classAndGrade,
            "year": this.year,
            "term": classTerm
        }

        $.ajax({
            data: data,
            type: "post",
            url: url + "student_sport_report",
            success: function(dataRes) {
                if (dataRes.header.code = "200") {
                    //f(dataRes.data.report_list)
                    console.log("dataRes",dataRes)
                    if(dataRes.data.report_list.length==0){
                        $("#hasNoData").show();
                        $("#hasData").hide();
                        $("#noDataList").show();

                    }else{
                        $("#noDataList").hide();
                        $("#hasNoData").hide();
                        $("#hasData").show();
                          console.log(dataRes.data.report_list[0].item_list)
                        var newCrease = dataRes.data.report_list[0].item_list.length;
                        var creaseHtml = "";
                        var scoreHtml = "";
                        var infoHtml = "";
                        creaseHtml += '<th rowspan="2">序号</th><th rowspan="2">学籍号码</th><th rowspan="2">姓名</th><th rowspan="2">性别</th>';
                        for (var i = 0; i < newCrease; i++) {
                            creaseHtml += ' <th colspan = "3" >' + dataRes.data.report_list[0].item_list[i].item + '</th>';
                        }
                        $($("table tr")[0]).html(creaseHtml);
                        for(var j=0;j<newCrease;j++){
                            scoreHtml+='<td>成绩</td><td>得分</td><td>等级</td>';
                        }
                        $($("table tr")[1]).html(scoreHtml);
                        //得到学生的信息

                        for(var j=0;j<dataRes.data.report_list.length;j++){
                            tableHtml+='<tr><td>'+j+'</td><td>'+dataRes.data.report_list[j].student_id+'</td><td>'+dataRes.data.report_list[j].student_name+'</td><td>'+sexChange[dataRes.data.report_list[j].sex]+'</td>';
                            for(var k=0;k<dataRes.data.report_list[0].item_list.length;k++){
                                tableHtml+='<td>'+dataRes.data.report_list[j].item_list[k].record+'</td><td>'+dataRes.data.report_list[j].item_list[k].score+'</td><td>'+dataRes.data.report_list[j].item_list[k].level+'</td>'
                            }
                            tableHtml+='</tr>'
                        }

                        $("#hasData").append(tableHtml);
                    }

                    //console.log(dataRes.data.report_list[0].item_list[0].item)
                } else {
                    new Notice(dataRes.head.msg);
                    //alert(dataRes.head.msg)
                }
            }
        });




}
PersonHealth.prototype.getAllData = function() {
      var _this = this;
	$("#personHealthLook").on("click", function() {
        _this.getDefault();
	})

}



function classAndGrade(data) {
    var arrGradeAndClass = new Array();
    var commonData = dataArr;
    var temp = ""
    var gradeHtml = "";
    var classHtml = "";
    var classIdNum;
    for (var i = 0; i < data.length; i++) {
        var gradeName = commonData[data[i]].split(",")[0];
        var className = commonData[data[i]].split(",")[1];
        if (temp != gradeName) {
            gradeNameModel.push(gradeName);
            temp = gradeName;
        }
    }
    //年级排序
    gradeSort(gradeNameModel)
    for (var j = 0; j < gradeNameModel.length; j++) {
        gradeHtml += '<div><span>' + gradeNameModel[j] + '</span></div>'
        $("#gradeListHtmlId").html(gradeHtml);
        $("#choiceGrade").html($($("#gradeListHtmlId div")[0]).text());
    }
    for (var k = 0; k < gradeNameModel.length; k++) {
        classNameModel[k] = new Array();
        for (var j = 0; j < data.length; j++) {
            if (gradeNameModel[k] == commonData[data[j]].split(",")[0]) {

                classNameModel[k].push(commonData[data[j]].split(",")[1])
            }
        }
        classSort(classNameModel[k])
    }

    for (var i = 0; i < gradeNameModel.length; i++) {
        if ($("#choiceGrade").text() == gradeNameModel[i]) {
            classIdNum = i;
        }
    }
    var classHtml = "";
    for (var j = 0; j < classNameModel[classIdNum].length; j++) {
        classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
    }
    $("#classListHtmlId").html(classHtml);
    $("#choiceClass").html($($("#classListHtmlId div")[0]).text());
    defalutClassText = $($("#classListHtmlId div")[0]).text();
}

$(document).ready(function() {

	//          开始的时候获取班级和年级信息
	var url = getURL() + "get_user_class";
    var school=localStorage.getItem("schoolName");
    var name = localStorage.getItem("userName");
    var is_root = localStorage.getItem("is_root")
    var dataSchoolInfo ={"name":name,"schoolName":school,"is_root":is_root};
	var data = {
		"account":name
	};
    getStuInfo(dataSchoolInfo)
	$.ajax({
		data: data,
		type: "post",
		url: url,
		success: function(dataRes) {
            if(dataRes.header.code=="200"){
                if(dataRes.data.user_class.length!=0){
                    classAndGrade(dataRes.data.user_class);
                }else{
                    new Notice("暂无数据");
                }


                var user = localStorage.getItem("user");

                $(".userName").html(localStorage.getItem("userName"));
                console.log(localStorage.getItem("userName"))
                $(".schoolName").html(localStorage.getItem("schoolName"))
                var personList = new PersonHealth();
                personList.bindEvent();
                //学年
                personList.getPersonListYear();
                //学期
                personList.getPersonListTerm();
                //年级
                personList.getPersonListGrade();
                //班级
                personList.getPersonListClass();
                //获取默认的数据
                personList.getDefault();
                //获取数据
                personList.getAllData();
                //向服务器传输数据

            }else{
                new Notice(dataRes.header.msg)
            }


		},error:function(textStatus){
        console.log(textStatus)
    }
	});


})