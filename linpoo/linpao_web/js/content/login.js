/**
 * @author By Liyq
 * @date By 2016-01-18
 * @content for 登录
 */
//检测手机号码
function checkNum(num){
    var reg=new RegExp("^(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$");
    if(!reg.test(num)){
        return false;
    }else{
        return true;
    }
}
function login(){
	  var checkVal = false;
   $("#l_in").on("click",function(){
       var userNum = $("#user").val();
       var userPassWd = $("#passWd").val();
       if($("#check").prop("checked")){
          checkVal = true;
           localStorage.setItem("user",userNum);
           localStorage.setItem("passWd",userPassWd);
           localStorage.setItem("yes",checkVal);
       }
       else{
           localStorage.clear();
           checkVal = false;
       }
       if(userNum==""){
          // new Notice("请输入用户名")
          $("#inputNameOrPass").show();
           $("#inputNameOrPass").text("请输入用户名");
           return;
       }
       else if(userPassWd==""){
           $("#inputNameOrPass").show();
           $("#inputNameOrPass").text("请输入密码");
           return;
       }
       else{
           var url =getURL()+"school_login";
           var data={"user_name":userNum,"password":userPassWd}
           $.ajax({
               type : "post",
               url : url,
               data:data,
               success : function(data){
                   console.log(data)
                   if(data.header.code=="200"){
                      if(data.data.result=="0"){
                          $("#inputNameOrPass").hide();
                      	localStorage.setItem("schoolName",data.data.school);
                      	 localStorage.setItem("userName",userNum);
                      	 localStorage.setItem("schoolId",data.data.school_id);
                          localStorage.setItem("is_root",data.data.is_root);
                      	  window.location.href="personhealthyscore.html";
                      }
                      else{
                          $("#inputNameOrPass").show();
                          $("#inputNameOrPass").text(data.data.msg);

                      }
                   }else{
                       $("#inputNameOrPass").show();
                       $("#inputNameOrPass").text(data.header.msg);
                       return;
                   }

               }
           })


           //ajax请求
       }

   })
}

$(document).ready(function(){
	$(".input").find("input").focus(function(){
	$(this).attr("placeholder","").prev("span").css("opacity",1);
	$(this).prev("span").stop().animate({
		"top":"-8px",
		"font-size":"12px",
		"z-index":"100",
		"padding":"0 6px"
	},300)
});
$(".input").find("input").blur(function(){
	if($(this).val()==""){
		$(this).prev("span").stop().animate({
		"top":"16px",
		"font-size":"14px",
		"z-index":"-100",
		"padding":"0"
	},300);
	$(this).attr("placeholder",$(this).prev("span").text()).prev("span").css("opacity",0);
	}
})
$("[type=checkbox]").click(function(){
	if($(this).prop("checked")){
		$(this).prev("#img").css("background-image","url(img/select.png)")
	}else{
		$(this).prev("#img").css("background-image","url(img/unselect.png)")
	}
})
 if(localStorage.getItem("yes")){
         $("#user").val(localStorage.getItem("user"));
         $("#passWd").val(localStorage.getItem("passWd"));
        $("#img").css("background-image","url(img/select.png)");
        $(".phoneAndPass").each(function(){
        	var _this = this;
        	$(_this).css({
        	"top":"-8px",
		    "font-size":"12px",
		    "z-index":"100",
		    "padding":"0 6px",
		     "opacity": 1
         })
        })
     }else{
         localStorage.clear();
     }
    login();

})


