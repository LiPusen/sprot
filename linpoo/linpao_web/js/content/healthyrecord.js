/**
 * @author By lizhihu
 * @date By 2016-01-23
 */
function hideList() {
	$(".inputList").slideUp(500);
}
var gradeNameModel = [];
var classNameModel = [];
//默认班级
var defalutClassText = "";
var PersonHealth = function() {
	this.term = "";
	this.year = "";
	this.grade = "";
	this.classRoom = "";
	this.sex = "";
	this.stuName = "";
	$.easing.def = "easeInBack";

}
PersonHealth.prototype.getPersonListYear = function() {
	$("#choiceYear").html($($("#yearListId .list div")[0]).context.innerText);
	//学年
	$("#yearId").on("click", function(e) {
		hideList();
		if ($("#yearListId").css("display") != "block") {
			$("#yearListId").slideDown(500);
			$("#yearId img").attr("src", "img/moreup_gray.png");
		} else {
			$("#yearListId").slideUp(500);
			$("#yearId img").attr("src", "img/moredown_gray.png");
		}
        stopBubble(e)
		//event.stopPropagation();
	});
	$("#yearListId .list div").each(function(index, val) {
			return function() {
				$($("#yearListId .list div")[index]).click(function() {
					var _this = this;
					$("#choiceYear").html($(_this).context.innerText);
					$("#yearListId").slideUp(500);
					$("#yearId img").attr("src", "img/moredown_gray.png");
				})
			}(index)

		})
		//点击现在
	$("#nowYear").click(function() {
		$("#choiceYear").html($($("#yearListId .list div")[0]).context.innerText);
		$("#yearListId").slideUp(500);
		$("#yearId img").attr("src", "img/moredown_gray.png");
	})
	$(document).click(function(event) {
		$("#yearListId").slideUp(500);
		$("#yearId img").attr("src", "img/moredown_gray.png");
	})
};
PersonHealth.prototype.getPersonListTerm = function() {
	$("#choiceTerm").html($($("#termListId .list div")[0]).context.innerText);
	$("#termId").on("click", function(e) {
			hideList();
			if ($("#termListId").css("display") != "block") {
				$("#termListId").slideDown(500);
				$("#termId img").attr("src", "img/moreup_gray.png");
			} else {
				$("#termListId").slideUp(500);
				$("#termId img").attr("src", "img/moredown_gray.png");
			}
        stopBubble(e)
			//event.stopPropagation();
		})
		//选择学期
	$("#termListId .list div").each(function(index, val) {
		return function() {
			$($("#termListId .list div")[index]).click(function() {
				var _this = this;
				$("#choiceTerm").html($(_this).context.innerText);
				$("#termListId").slideUp(500);
				$("#termId img").attr("src", "img/moredown_gray.png");
			})
		}(index)

	})
	$(document).click(function(event) {
		$("#termListId").slideUp(500);
		$("#termId img").attr("src", "img/moredown_gray.png");
	})
}
PersonHealth.prototype.getPersonListGrade = function() {

		$("#gradeId").on("click", function(e) {

				hideList();
				if ($("#gradeListId").css("display") != "block") {
					$("#gradeListId").slideDown(500);
					$("#gradeId img").attr("src", "img/moreup.png");
				} else {
					$("#gradeListId").slideUp(500);
					$("#gradeId img").attr("src", "img/moredown_gray.png");
				}
            stopBubble(e)
				//event.stopPropagation();
			})
			//选择学期
		$("#gradeListId .list div").each(function(index, val) {
			return function() {
				$($("#gradeListId .list div")[index]).click(function() {

					var _this = this;
					$("#choiceGrade").html($(_this).context.innerText);

					$("#gradeListId").slideUp(500);
					$("#gradeId img").attr("src", "img/moredown_gray.png");

					//初始化另一个
					for (var i = 0; i < gradeNameModel.length; i++) {
						if ($("#choiceGrade").text() == gradeNameModel[i]) {
							classIdNum = i;
						}
					}
					var classHtml = "";
					for (var j = 0; j < classNameModel[classIdNum].length; j++) {
						classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
					}
					$("#classListHtmlId").html(classHtml);
					$("#choiceClass").html($($("#classListHtmlId div")[0]).text());
				})
			}(index)


		})
		$(document).click(function(event) {
			$("#gradeListId").slideUp(500);
			$("#gradeId img").attr("src", "img/moredown_gray.png");
		})
	}
	//班级
PersonHealth.prototype.getPersonListClass = function() {

	$("#choiceClass").html($($("#classListId .list div")[0]).context.innerText);
	$("#classId").on("click", function(e) {


			for (var i = 0; i < gradeNameModel.length; i++) {
				if ($("#choiceGrade").text() == gradeNameModel[i]) {
					classIdNum = i;
				}
			}
			var classHtml = "";
			for (var j = 0; j < classNameModel[classIdNum].length; j++) {
				classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
			}

			$("#classListHtmlId").html(classHtml);
			hideList();
			if ($("#classListId").css("display") != "block") {
				$("#classListId").slideDown(500);
				$("#classId img").attr("src", "img/moreup_gray.png");
			} else {
				$("#classListId").slideUp(500);
				$("#classId img").attr("src", "img/moredown_gray.png");
			}
        stopBubble(e)
			//event.stopPropagation();
			$("#classListId .list div").each(function(index, val) {
				return function() {
					$($("#classListId .list div")[index]).click(function() {
						var _this = this;
						$("#choiceClass").html($(_this).context.innerText);
						$("#classListId").slideUp(500);
						$("#classId img").attr("src", "img/moredown_gray.png");
					})
				}(index)

			})
			$(document).click(function(event) {
				$("#classListId").slideUp(500);
				$("#classId img").attr("src", "img/moredown_gray.png");
			})
		})
		//选择学期

}
function getChildHtml(childDataList){
    var stu_name = ""
    var class_name = "";
    var sex_name = "";
    var stu_id = "";
    var year_name = "";
    var stu_age = "";
    var stu_nation = "";
    var term_name = "";
    var student_infoAddr = "";
    var student_infoList = "";
    $("#childList li").each(function(index,value){
        var _this = this;
        if(childDataList.length>0){
            if($(_this).hasClass("active")){
                stu_name = $(_this).context.innerText;
                $("#stuInfoAddrNoData").hide();
                $("#stuInfoAddr").show();
                console.log(childDataList)
                for(var i=0;i< childDataList.length;i++){
                    if($.trim(stu_name) == $.trim(childDataList[i][0].student_name)){
                        class_name = dataArr[childDataList[i][0].class_id];
                        sex_name = sexChange[childDataList[i][0].sex];
                        stu_id = childDataList[i][0].student_id;
                        year_name =childDataList[i][0].year;
                        stu_age = childDataList[i][0].birth;
                        stu_nation = NationArr[childDataList[i][0].nationality];
                        term_name =dataTermBrr[childDataList[i][0].term];
                        student_infoAddr+='<tr><td>姓名</td><td>'+stu_name+'</td><td>班级</td><td>'+class_name+'</td></tr>';
                        student_infoAddr+='<tr><td>性别</td><td>'+sex_name+'</td><td>出生日期</td><td>'+stu_age+'</td></tr>';
                        student_infoAddr+='<tr><td>学籍号</td><td>'+stu_id+'</td><td>民族</td><td>'+stu_nation+'</td></tr>';
                        student_infoAddr+='<tr><td>学年</td><td>'+year_name+'</td><td>学期</td><td>'+term_name+'</td></tr>';

                        $("#stuInfoAddr").html(student_infoAddr);
                        student_infoList+='<tr><th colspan="6" style="border-top:none ;">健康指标体系</th></tr>';
                        student_infoList+='<tr><td>健康指标</td><td>单项指标</td><td>成绩</td><td>得分</td><td>国家级</td><td>健康指数(地区)</td></tr>';
                        var td=childDataList[i][0];
                        for(var a=0;a<td.form.length;a++){
                            if(a==0){
                                student_infoList+='<tr><td rowspan="'+td.form.length+'" colspan="1">身体形态</td><td colspan="1">'+td.form[a].item+'('+td.form[a].unit+')</td><td colspan="1">'+td.form[a].score+'</td><td colspan="1">'+td.form[a].record+'</td><td rowspan="'+td.form.length+'" colspan="1"></td><td rowspan="'+td.form.length+'" colspan="1"></td></tr>';
                            }else{
                                student_infoList+='<tr><td colspan="1">'+td.form[a].item+'('+td.form[a].unit+')</td><td colspan="1">'+td.form[a].score+'</td><td colspan="1">'+td.form[a].record+'</td></tr>';
                            }
                        }
                        for(var a=0;a<td.enginery.length;a++){

                                if(a==0){
                                    student_infoList+='<tr><td rowspan="'+td.enginery.length+'" colspan="1">身体机能</td><td colspan="1">'+td.enginery[a].item+'('+td.enginery[a].unit+')</td><td colspan="1">'+td.enginery[a].score+'</td><td colspan="1">'+td.enginery[a].record+'</td><td rowspan="" colspan="1"></td><td rowspan="" colspan="1"></td></tr>';
                                }else{
                                    student_infoList+='<tr><td colspan="1">'+td.enginery[a].item+'('+td.enginery[a].unit+')</td><td colspan="1">'+td.enginery[a].score+'</td><td colspan="1">'+td.enginery[a].record+'</td><td rowspan="" colspan="1"></td><td rowspan="" colspan="1"></td></tr>';
                                }

                        }
                        for(var a=0;a<td.stamina.length;a++){

                            if(a==0){
                                student_infoList+='<tr><td rowspan="'+td.stamina.length+'" colspan="1">身体机能</td><td colspan="1">'+td.stamina[a].item+'('+td.stamina[a].unit+')</td><td colspan="1">'+td.stamina[a].score+'</td><td colspan="1">'+td.stamina[a].record+'</td><td rowspan="" colspan="1"></td><td rowspan="" colspan="1"></td></tr>';
                            }else{
                                student_infoList+='<tr><td colspan="1">'+td.stamina[a].item+'('+td.stamina[a].unit+')</td><td colspan="1">'+td.stamina[a].score+'</td><td colspan="1">'+td.stamina[a].record+'</td><td rowspan="" colspan="1"></td><td rowspan="" colspan="1"></td></tr>';
                            }

                        }
                        student_infoList+='<tr><td colspan="3">总评</td><td></td><td></td><td></td></tr><tr><th colspan="6">运动处方</th></tr><tr><td colspan="6">';
                        student_infoList+='<tr><td colspan="6"> <div class="imgCode"> <img src="img/code.png"/><span>运动指导App</span></div>  <div style="width: 100%;padding: 20px;"><p align="left" style="color: red;text-indent: 15px;">建议扫描安装“运动指导”手机客户端，可以科学有效的协助孩子的日常训练。</p><p align="right" style="margin-top: 100px;color:rgba(50,20,30,.8);">签名：<span style="display:inline-block;width: 120px;border-bottom: 1px solid #ccc;"></span></p></div> </td></tr>'
                        $("#healthItemList").html(student_infoList);

                    }else{
                        continue;
                    }
                }
            }

        }
        else{
            $("#stuInfoAddrNoData").show();
            $("#stuInfoAddr").hide();
        }


    })
}
function getChild(data){
   var childHtml = "";
    var childDataList = data.data.all_student;
    if(childDataList.length>0){
        $("#studentListId").hide();
        for(var i=0;i<childDataList.length;i++){
            childHtml+='<li><a href="javascript:void(0)" class="stu-name">'+childDataList[i][0].student_name+'<span class="stu-ico"></span> </a></li>';

        }
        $("#childList").html(childHtml);
    }else{
        $("#studentListId").show();
        $("#childList").html("");
        $("#stuInfoAddrNoData").show();
        $("#stuInfoAddr").hide();
    }

    //默认第一个选中
    $( $("#childList li")[0]).attr("class","active");
    $( $("#childList li")[0]).find("span").attr("class","stu-ico acitve");
    $( $("#childList li")[0]).find("a").css("color","red")
    getChildHtml(childDataList);
    //点击事件
    var aArr = $(".stu-name");
    var stuName = "";
    aArr.on("click", function() {
        var _this = this;
        $("#childList").find("li").each(function(index){
            return function(index){
                $($("#childList").find("li")[index]).find("a").css("color","#767676");
            }(index)
        })
        $(".stu-ico").attr("class", "stu-ico");
        aArr.parent().attr("class", "");
        $(_this).find(".stu-ico").attr("class", "stu-ico acitve");
        $(_this).parent().attr("class", " active");
       $(_this).css("color","red")
        getChildHtml(childDataList);
    })






}
PersonHealth.prototype.defalutData = function() {
	this.year = $("#choiceYear").text();
	this.term = $("#choiceTerm").text();
	this.grade = $("#choiceGrade").text();
	this.classRoom = $("#choiceClass").text();
	this.sex = $('#checkSex input[type="radio"]:checked ').val();
	$("#title").html(this.year + "学年" + this.term + this.grade + this.classRoom + "体质健康评分表");

    var classTmp = this.grade+","+this.classRoom;
    var yearName = this.year;
    var termName = dataTerm[this.term];
    var className = dataBrr[classTmp];
    var sexName = this.sex;
    if(this.sex==3){
        sexName=""
    }else{
        sexName = this.sex;
    }
    //2016 1 1102 全部有数据
    var data1 = {
        "year": yearName,
        "term":termName,
        "class_id":className,
        "sex":sexName
    };
    $.ajax({
        data: data1,
        type: "post",
        url: getURL()+"health_record",
        success: function(dataRes) {
            //getFirstData(dataRes)
            if(dataRes.header.code=="200"){
                getChild(dataRes)
            }else{
                new Notice(dataRes.head.msg);
                //alert(dataRes.header.msg)
            }
            console.log(dataRes)
        }
    })



}
PersonHealth.prototype.getAllData = function() {

	var _this = this;
	$("#personHealthLook").on("click", function() {
		_this.defalutData();
	})

}
PersonHealth.prototype.getLeft = function() {

	/*var aArr = $(".stu-name");
	var stuName = "";
	stuName = $($(".stu-name")[0]).context.innerText;
	aArr.on("click", function() {
			var _this = this;
			$(".stu-ico").attr("class", "stu-ico");
			aArr.parent().attr("class", "");
			$(_this).find(".stu-ico").attr("class", "stu-ico acitve");
			$(_this).parent().attr("class", " active");
			stuName = $(_this).context.innerText;

		})*/
		//ajax进行表格渲染
}

function classAndGrade(data) {
	var arrGradeAndClass = new Array();
	var commonData = dataArr;
	var temp = ""
	var gradeHtml = "";
	var classHtml = "";
	var classIdNum;
	for (var i = 0; i < data.length; i++) {
		var gradeName = commonData[data[i]].split(",")[0];
		var className = commonData[data[i]].split(",")[1];
		if (temp != gradeName) {
			gradeNameModel.push(gradeName);
			temp = gradeName;

		}
	}
    gradeSort(gradeNameModel)
	for (var j = 0; j < gradeNameModel.length; j++) {
		gradeHtml += '<div><span>' + gradeNameModel[j] + '</span></div>'
		$("#gradeListHtmlId").html(gradeHtml);
		$("#choiceGrade").html($($("#gradeListHtmlId div")[0]).text());
	}
	for (var k = 0; k < gradeNameModel.length; k++) {
		classNameModel[k] = new Array();
		for (var j = 0; j < data.length; j++) {
			if (gradeNameModel[k] == commonData[data[j]].split(",")[0]) {

				classNameModel[k].push(commonData[data[j]].split(",")[1])
			}
		}
        classSort(classNameModel[k])
	}
    //classSort(classNameModel[k])
	for (var i = 0; i < gradeNameModel.length; i++) {
		if ($("#choiceGrade").text() == gradeNameModel[i]) {
			classIdNum = i;
		}
	}
	var classHtml = "";
	for (var j = 0; j < classNameModel[classIdNum].length; j++) {
		classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
	}
	$("#classListHtmlId").html(classHtml);
	console.log(classHtml)
	$("#choiceClass").html($($("#classListHtmlId div")[0]).text());
	defalutClassText = $($("#classListHtmlId div")[0]).text();
}

$(document).ready(function() {
    var school=localStorage.getItem("schoolName");
    var name = localStorage.getItem("userName")
    var is_root = localStorage.getItem("is_root")
    var dataSchoolInfo ={"name":name,"schoolName":school,"is_root":is_root};
    getStuInfo(dataSchoolInfo)
	var url = getURL() + "get_user_class";
	var data = {
		"account": name
	};

$.ajax({
		data: data,
		type: "post",
		url: url,
		success: function(dataRes) {
            if(dataRes.header.code=="200"){
                if(dataRes.data.user_class.length!=0){
                    classAndGrade(dataRes.data.user_class)
                }else{
                    new Notice("获取数据失败");
                    //alert("获取数据失败")
                }

                $(".userName").html(localStorage.getItem("userName"));
                console.log(localStorage.getItem("userName"))
                $(".schoolName").html(localStorage.getItem("schoolName"))
                var personList = new PersonHealth();

                //学年
                personList.getPersonListYear();
                //学期
                personList.getPersonListTerm();
                //年级
                personList.getPersonListGrade();
                //班级
                personList.getPersonListClass();
                //获取数据
                personList.defalutData();
                //向服务器传输数据
                personList.getAllData();
                personList.getLeft();
            }else{
                new Notice(dataRes.header.msg);
                //alert(dataRes.header.msg)
            }

		}
	})

	//跳转


})