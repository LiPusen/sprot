/**
 * @author By lizhihu
 * @date By 2016-01-25
 */
function hideList() {
	$(".inputList").slideUp(500);
}
var gradeNameModel = [];
var classNameModel = [];
//默认班级
var defalutClassText = "";
var PersonHealth = function() {
	this.term = "";
	this.year = "";
	this.grade = "";
	this.classRoom = "";
	$.easing.def = "easeInBack";

}
PersonHealth.prototype.getPersonListYear = function() {
	//默认第一个
	$("#choiceYear").html($($("#yearListId .list div")[0]).context.innerText);
	console.log($($("#yearListId .list div")[0]).context.innerText)
		//学年
	$("#yearId").on("click", function(e) {
		hideList();
		if ($("#yearListId").css("display") != "block") {
			$("#yearListId").slideDown(500);
			$("#yearId img").attr("src", "img/moreup_gray.png");
		} else {
			$("#yearListId").slideUp(500);
			$("#yearId img").attr("src", "img/moredown_gray.png");
		}
        stopBubble(e)
		//event.stopPropagation();
	});
	$("#yearListId .list div").each(function(index, val) {
			return function() {
				$($("#yearListId .list div")[index]).click(function() {
					var _this = this;
					$("#choiceYear").html($(_this).context.innerText);
					$("#yearListId").slideUp(500);
					$("#yearId img").attr("src", "img/moredown_gray.png");
				})
			}(index)

		})
		//点击现在
	$("#nowYear").click(function() {
		$("#choiceYear").html($($("#yearListId .list div")[0]).context.innerText);
		$("#yearListId").slideUp(500);
		$("#yearId img").attr("src", "img/moredown_gray.png");
	})
	$(document).click(function(event) {
		$("#yearListId").slideUp(500);
	})
};

//数据统计
PersonHealth.prototype.bindEvent = function() {
	var claerTime, claerTime1;
	$(".biaoNow").hover(function() {
		clearTimeout(claerTime1);
		clearTimeout(claerTime);
		if ($(".biaoList").css("display") != "block") {
			$(".biaoList").slideDown('500');
			$($(".biaoNow").find("img")[0]).attr("src", "img/moreup_gray.png");
		}
	}, function(event) {
		claerTime = setTimeout(function() {
			$(".biaoList").slideUp('500');
			$($(".biaoNow").find("img")[0]).attr("src", "img/moredown_gray.png");
		}, 500)

	});
	$(".biaoList").hover(function() {
		clearTimeout(claerTime);
		clearTimeout(claerTime1);
		$(".biaoList").css("display", "block");
	}, function() {
		claerTime1 = setTimeout(function() {
			$(".biaoList").slideUp('500');
			$($(".biaoNow").find("img")[0]).attr("src", "img/moredown_gray.png");
		}, 500)
	})

}
PersonHealth.prototype.getPersonListGrade = function() {

		$("#choiceGrade").html($($("#gradeListId .list div")[0]).context.innerText);
		$("#gradeId").on("click", function(e) {
				hideList();
				if ($("#gradeListId").css("display") != "block") {
					$("#gradeListId").slideDown(500);
					$("#gradeId img").attr("src", "img/moreup_gray.png");
				} else {
					$("#gradeListId").slideUp(500);
					$("#gradeId img").attr("src", "img/moredown_gray.png");
				}
            stopBubble(e)
				//event.stopPropagation();
			})
			//选择学期
		$("#gradeListId .list div").each(function(index, val) {
			return function() {
				$($("#gradeListId .list div")[index]).click(function() {
					var _this = this;
					$("#choiceGrade").html($(_this).context.innerText);
					$("#gradeListId").slideUp(500);
					$("#gradeId img").attr("src", "img/moredown_gray.png");
					var _this = this;
					$("#choiceGrade").html($(_this).context.innerText);

					$("#gradeListId").slideUp(500);
					$("#gradeId img").attr("src", "img/moredown_gray.png");

					//初始化另一个
					for (var i = 0; i < gradeNameModel.length; i++) {
						if ($("#choiceGrade").text() == gradeNameModel[i]) {
							classIdNum = i;
						}
					}
					var classHtml = "";
					for (var j = 0; j < classNameModel[classIdNum].length; j++) {
						classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
					}
					$("#classListHtmlId").html(classHtml);
					$("#choiceClass").html($($("#classListHtmlId div")[0]).text());

				})
			}(index)

		})
		$(document).click(function(event) {
			$("#gradeListId").slideUp(500);
			$("#gradeId img").attr("src", "img/moredown_gray.png");
		})
	}
	//班级
PersonHealth.prototype.getPersonListClass = function() {
	$("#choiceClass").html($($("#classListId .list div")[0]).context.innerText);
	$("#classId").on("click", function(e) {
			hideList();
			if ($("#classListId").css("display") != "block") {
				$("#classListId").slideDown(500);
				$("#classId img").attr("src", "img/moreup_gray.png");
			} else {
				$("#classListId").slideUp(500);
				$("#classId img").attr("src", "img/moredown_gray.png");
			}
        stopBubble(e)
			//event.stopPropagation();
        $("#classListId .list div").each(function(index, val) {
            return function() {
                $($("#classListId .list div")[index]).click(function() {
                    var _this = this;
                    $("#choiceClass").html($(_this).context.innerText);
                    $("#classListId").slideUp(500);
                    $("#classId img").attr("src", "img/moredown_gray.png");
                })
            }(index)

        })
		})
		//选择学期

	$(document).click(function(event) {
		$("#classListId").slideUp(500);
		$("#classId img").attr("src", "img/moredown_gray.png");
	})
}
function getHealthTable(data){
    var tableHtml = "";
    var dataDetail = data.data.sport_item_rate;
    for(var i=0;i<dataDetail.length;i++){
        tableHtml+='<tr><td>'+dataDetail[i].item+'</td><td>'+dataDetail[i].one+'</td><td>'+dataDetail[i].one_rate.toFixed(2)+"%"+'</td><td>'+dataDetail[i].two+'</td><td>'+dataDetail[i].two_rate.toFixed(2)+"%"+'</td><td>'+dataDetail[i].three+'</td><td>'+dataDetail[i].three_rate.toFixed(2)+"%"+'</td><td>'+dataDetail[i].zero+'</td><td>'+dataDetail[i].zero_rate.toFixed(2)+"%"+'</td></tr>';
    }
    $("#hasData").append(tableHtml);
}
PersonHealth.prototype.defalutData = function() {
	this.year = $("#choiceYear").text();
	this.grade = $("#choiceGrade").text();
	this.classRoom = $("#choiceClass").text();
    $("#gradeTitle").html(this.grade+this.classRoom);
	$("#title").html("测试合作学校" + this.year + "学年" + this.grade + this.classRoom + "健康指标数据统计")
   /* var url = getURL() + "get_user_class";*/
    //班级是1年级2班 2016有数据
    var classId = this.grade + "," + this.classRoom;
    classId = dataBrr[classId];
    var data1 = {
        "year": this.year,
        "class_id":classId
    };
    $.ajax({
        data: data1,
        type: "post",
        url: getURL()+"sport_item_report_rate",
        success: function(dataRes) {
            console.log(dataRes)
            if(dataRes.data.sport_item_rate.length==0){
                $("#noData").show();
                $("#hasNoData").show();
                $("#hasData").hide();
                setWidth(".thin-pro",thinPro,".thin-per");
                setWidth(".normal-pro",normalPro,".normal-per");
                setWidth(".overweight-pro",overweightPro,".overweight-per");
                setWidth(".fat-pro",fatPro,".fat-per");
                $(".test-num").text(0+"人");
                $(".thin-text-num").text(thinNum+"人");
                $(".normal-test-num").text(normalNum+"人");
                $(".overweight-test-num").text(overweightNum+"人");
                $(".fat-test-num").text(fatNum+"人");
            }else{
                $("#noData").hide();
                $("#hasNoData").hide();
                $("#hasData").show();
                getHealthTable(dataRes);
                getFirstData(dataRes)


            }


        }
    });



}
PersonHealth.prototype.getAllData = function() {

		var _this = this;
    this.year = $("#choiceYear").text();
    this.grade = $("#choiceGrade").text();
    this.classRoom = $("#choiceClass").text();
    $("#gradeTitle").html(this.grade+this.classRoom);
		$("#checkDataFull").on("click", function() {
            $("#hasData  tr:not(:first)").empty("");
			_this.defalutData();
		})

	}
	//进行数据拆分和数据渲染
function classAndGrade(data) {
	var arrGradeAndClass = new Array();
	var commonData = dataArr;
	var temp = ""
	var gradeHtml = "";
	var classHtml = "";
	var classIdNum;
	for (var i = 0; i < data.length; i++) {
		var gradeName = commonData[data[i]].split(",")[0];
		var className = commonData[data[i]].split(",")[1];
		if (temp != gradeName) {
			gradeNameModel.push(gradeName);
			temp = gradeName;

		}
	}
    gradeSort(gradeNameModel)
	for (var j = 0; j < gradeNameModel.length; j++) {
		gradeHtml += '<div><span>' + gradeNameModel[j] + '</span></div>'
		$("#gradeListHtmlId").html(gradeHtml);
		$("#choiceGrade").html($($("#gradeListHtmlId div")[0]).text());
	}
	for (var k = 0; k < gradeNameModel.length; k++) {
		classNameModel[k] = new Array();
		for (var j = 0; j < data.length; j++) {
			if (gradeNameModel[k] == commonData[data[j]].split(",")[0]) {

				classNameModel[k].push(commonData[data[j]].split(",")[1])
			}
		}
        classSort(classNameModel[k]);
	}

	for (var i = 0; i < gradeNameModel.length; i++) {
		if ($("#choiceGrade").text() == gradeNameModel[i]) {
			classIdNum = i;
		}
	}
	var classHtml = "";
	for (var j = 0; j < classNameModel[classIdNum].length; j++) {
		classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
	}
	$("#classListHtmlId").html(classHtml);
	console.log(classHtml)
	$("#choiceClass").html($($("#classListHtmlId div")[0]).text());
	defalutClassText = $($("#classListHtmlId div")[0]).text();
}
//初始化值肥胖的值
function getFirstData(data){
	            var charBox=$(".m-chart");
				var boxWidth=charBox.width();
				var marginR=(boxWidth-554)/3;
				var charList=charBox.find(".chart");
				charList.css("margin-right",marginR+"px");
				$(charList[3]).css("margin-right","0px");
				var fatNum,normalNum,overweightNum,thinNum,contNum,thinPro,normalPro,fatPro,overweightPro;
                var isHasFat = false;
				for(var i=0;i<data.data.sport_item_rate.length;i++){
					if(data.data.sport_item_rate[i].item_id=="7"){
                        isHasFat=true;
						thinNum=data.data.sport_item_rate[i].zero;;//偏瘦人数
						normalNum=data.data.sport_item_rate[i].one;;//正常人数
						overweightNum=data.data.sport_item_rate[i].two;//超重人数
						fatNum=data.data.sport_item_rate[i].three;//肥胖人数
						contNum=data.data.sport_item_rate[i].total;
                        thinPro=per(thinNum,contNum);
                        normalPro=per(normalNum,contNum);
                        overweightPro=per(overweightNum,contNum);
                        fatPro=per(fatNum,contNum);
                        setWidth(".thin-pro",thinPro,".thin-per");
                        setWidth(".normal-pro",normalPro,".normal-per");
                        setWidth(".overweight-pro",overweightPro,".overweight-per");
                        setWidth(".fat-pro",fatPro,".fat-per");
                        $("#contNum").text(contNum+"人");
                        $(".thin-text-num").text(thinNum+"人");
                        $(".normal-test-num").text(normalNum+"人");
                        $(".overweight-test-num").text(overweightNum+"人");
                        $(".fat-test-num").text(fatNum+"人");
					}else{
						continue;
					}
				}
				if(!isHasFat){
                    setWidth(".thin-pro",0,".thin-per");
                    setWidth(".normal-pro",0,".normal-per");
                    setWidth(".overweight-pro",0,".overweight-per");
                    setWidth(".fat-pro",0,".fat-per");
                    $(".test-num").text(0+"人");
                    $(".thin-text-num").text(0+"人");
                    $(".normal-test-num").text(0+"人");
                    $(".overweight-test-num").text(0+"人");
                    $(".fat-test-num").text(0+"人");
                }
				function per(num,contNum){
//					return (Math.round((num / contNum * 10000) / 100) + "%");
					return (Math.round((num / contNum * 10000) / 100) );
				}
				function setWidth(sel,pro,sel1){
					var i=0;
					var timer=setInterval(function(){
						if(i==pro){
							clearInterval(timer);
						}
						$(sel1).text(i+"%");
						i++;
					},10);
					$(sel).css("width",pro+"%");
				}

}
$(document).ready(function() {
    var school=localStorage.getItem("schoolName");
    var name = localStorage.getItem("userName")
    var is_root = localStorage.getItem("is_root")
    var dataSchoolInfo ={"name":name,"schoolName":school,"is_root":is_root};
    getStuInfo(dataSchoolInfo)
    var url = getURL() + "get_user_class";
	var data = {
		"account": name
	};
	$.ajax({
		data: data,
		type: "post",
		url: url,
		success: function(dataRes) {
            console.log("dataFull",dataRes);
            if(dataRes.header.code=="200"){
               if(dataRes.data.user_class.length==0){
                   new Notice("获取数据失败")
                   //alert("获取数据失败")
               }else{
                   classAndGrade(dataRes.data.user_class);
               }
                //初始化数据
                var dataHeal = new PersonHealth();
                dataHeal.bindEvent();
                //学年
                dataHeal.getPersonListYear();
                //学期
                dataHeal.getPersonListGrade();
                //班级
                dataHeal.getPersonListClass();
                //获取默认值
                dataHeal.defalutData();
                //点击后数据展示
                dataHeal.getAllData();
            }else{
                new Notice(dataRes.header.msg)
               // alert(dataRes.header.msg)
            }

			//点击个人健康评分表
              

		}

	})
});