/**
 * @date 2016-02-01
 * @author By Lyq
 * @content For 日常训练
 */
window.onload = function() {
    var gradeNameModel = [];
    var classNameModel = [];
    var tableHtml = ""
    var PersonHealth = function() {
        this.term = "";
        this.year = "";
        this.grade = "";
        this.classRoom = "";
        this.sex = "";
        $.easing.def = "easeInBack";

    }
    PersonHealth.prototype.getPersonListGrade = function() {

        $("#choiceGrade").html($($("#gradeListId .list div")[0]).context.innerText);
        $("#gradeId").on("click", function(e) {
            if ($("#gradeListId").css("display") != "block") {
                $("#gradeListId").slideDown(500);
                $("#gradeId img").attr("src", "img/moreup_gray.png");
            } else {
                $("#gradeListId").slideUp(500);
                $("#gradeId img").attr("src", "img/moredown_gray.png");
            }
            stopBubble(e)
           // event.stopPropagation();
        })
        //选择学期
        $("#gradeListId .list div").each(function(index, val) {
            return function() {
                $($("#gradeListId .list div")[index]).click(function() {
                    var _this = this;
                    $("#choiceGrade").html($(_this).context.innerText);
                    $("#gradeListId").slideUp(500);
                    $("#gradeId img").attr("src", "img/moredown_gray.png");
                    var _this = this;
                    $("#choiceGrade").html($(_this).context.innerText);

                    $("#gradeListId").slideUp(500);
                    $("#gradeId img").attr("src", "img/moredown_gray.png");

                    //初始化另一个
                    for (var i = 0; i < gradeNameModel.length; i++) {
                        if ($("#choiceGrade").text() == gradeNameModel[i]) {
                            classIdNum = i;
                        }
                    }
                    var classHtml = "";
                    for (var j = 0; j < classNameModel[classIdNum].length; j++) {
                        classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
                    }
                    $("#classListHtmlId").html(classHtml);
                    $("#choiceClass").html($($("#classListHtmlId div")[0]).text());

                })
            }(index)

        })
        $(document).click(function(event) {
            $("#gradeListId").slideUp(500);
            $("#gradeId img").attr("src", "img/moredown_gray.png");
        })
    }
    //班级
    PersonHealth.prototype.getPersonListClass = function() {
        $("#choiceClass").html($($("#classListId .list div")[0]).context.innerText);
        $("#classId").on("click", function(e) {
            if ($("#classListId").css("display") != "block") {
                $("#classListId").slideDown(500);
                $("#classId img").attr("src", "img/moreup_gray.png");
            } else {
                $("#classListId").slideUp(500);
                $("#classId img").attr("src", "img/moredown_gray.png");
            }
            stopBubble(e)
            //event.stopPropagation();
            $("#classListId .list div").each(function(index, val) {
                return function() {
                    $($("#classListId .list div")[index]).click(function() {
                        var _this = this;
                        $("#choiceClass").html($(_this).context.innerText);
                        $("#classListId").slideUp(500);
                        $("#classId img").attr("src", "img/moredown_gray.png");
                    })
                }(index)

            })
        })
        //选择学期
        $("#classListId .list div").each(function(index, val) {
            return function() {
                $($("#classListId .list div")[index]).click(function() {
                    var _this = this;
                    $("#choiceClass").html($(_this).context.innerText);
                    $("#classListId").slideUp(500);
                    $("#classId img").attr("src", "img/moredown_gray.png");
                })
            }(index)

        })
        $(document).click(function(event) {
            $("#classListId").slideUp(500);
            $("#classId img").attr("src", "img/moredown_gray.png");
        })
    }
    function classAndGrade(data) {
        var arrGradeAndClass = new Array();
        var commonData = dataArr;
        var temp = ""
        var gradeHtml = "";
        var classHtml = "";
        var classIdNum;
        for (var i = 0; i < data.length; i++) {
            var gradeName = commonData[data[i]].split(",")[0];
            var className = commonData[data[i]].split(",")[1];
            if (temp != gradeName) {
                gradeNameModel.push(gradeName);
                temp = gradeName;

            }
        }
        gradeSort(gradeNameModel)
        for (var j = 0; j < gradeNameModel.length; j++) {
            gradeHtml += '<div><span>' + gradeNameModel[j] + '</span></div>'
            $("#gradeListHtmlId").html(gradeHtml);
            $("#choiceGrade").html($($("#gradeListHtmlId div")[0]).text());
        }
        for (var k = 0; k < gradeNameModel.length; k++) {
            classNameModel[k] = new Array();
            for (var j = 0; j < data.length; j++) {
                if (gradeNameModel[k] == commonData[data[j]].split(",")[0]) {

                    classNameModel[k].push(commonData[data[j]].split(",")[1])
                }
            }
            classSort(classNameModel[k])
        }

        for (var i = 0; i < gradeNameModel.length; i++) {
            if ($("#choiceGrade").text() == gradeNameModel[i]) {
                classIdNum = i;
            }
        }
        var classHtml = "";
        for (var j = 0; j < classNameModel[classIdNum].length; j++) {
            classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
        }
        $("#classListHtmlId").html(classHtml);
        $("#choiceClass").html($($("#classListHtmlId div")[0]).text());
        defalutClassText = $($("#classListHtmlId div")[0]).text();
    }
    var url = getURL() + "get_user_class";
    var school=localStorage.getItem("schoolName");
    var name = localStorage.getItem("userName")
    var is_root = localStorage.getItem("is_root")
    var dataSchoolInfo ={"name":name,"schoolName":school,"is_root":is_root};
    var data = {
        "account": name
    };

    getStuInfo(dataSchoolInfo)
    $.ajax({
        data: data,
        type: "post",
        url: url,
        success: function (dataRes) {
            if(dataRes.header.code=="200"){
                if(dataRes.data.user_class.length!=0){
                    classAndGrade(dataRes.data.user_class);
                }else{
                    new Notice(dataRes.header.msg)
                  // alert("获取数据失败")
                }
                var detail = new PersonHealth();
                detail.getPersonListGrade();
                detail.getPersonListClass();
                var grade = $("#choiceGrade").text();
                var classRoom = $("#choiceClass").text();
                var classId = grade + "," + classRoom;
                classId = dataBrr[classId];

                new Daily(document.getElementById("day_chart"),classId);

                /*var comp = $("#complate")
                 console.log(comp)
                 if(comp=="undefined%"){
                 $("#complate").html("暂无数据")
                 }*/
                $("#checkDetail").on("click",function(){
                    var grade = $("#choiceGrade").text();
                    var classRoom = $("#choiceClass").text();
                    var classId = grade + "," + classRoom;
                    classId = dataBrr[classId];
                    new Daily(document.getElementById("day_chart"),classId);
                    console.log("copm111::",$("#complate").html())
                });
            }else{
                new Notice(dataRes.header.msg)
                //alert(dataRes.header.msg)
            }




        }
    });
	function Daily(ele,classId) {
		this.ele = ele;
		this.sbool = false;
		this.smooth = true;
		this.data = [];
		this.arr = [];
		this.arr_x = [];
        this.class_id = classId;
		this.init();
		this.something();
	}

	Daily.prototype = {
		constructor: Daily,

		init: function() {
			this.days_one();
		},
		ops: function() {
			var that=this;
			var opt = {
				tooltip: {
					trigger: 'axis',
					axisPointer: {
						lineStyle: {
							color: "#ff7247"
						}
					},
					formatter: "<div class='day_tip'><div class='day_sixh'>{b0}</div><div class='day_sixc'>完成率</div><div class='day_ty' id='complate'>{c0}%</div></div>"
				},
				grid: {
					top: "6",
					left: '6',
					right: '6',
					bottom: '5',
					borderColor: "#f0f0f0",
					containLabel: true
				},
				xAxis: [{
					type: 'category',
					boundaryGap: false,
					axisLabel: {
						show: that.sbool,
						interval: that.fsyle || null,
						textStyle: {
							color: "#767676"
						},
						formatter: function(value,key){
							if (key != 0) {
							    value=value.slice(5);
							    return value;
							}else{
								return value;
							}
						}
					},
					axisLine: {
						show: true,
						lineStyle: {
							color: "#f0f0f0",
							type: 'solid',
							opacity: 1
						}
					},
					data: that.arr_x,
					axisTick: {
						show: false
					}
				}],
				yAxis: [{
					type: 'value',
					splitNumber: 10,
					axisLabel: {
						show: true,
						interval: 1,
						inside: true,
						formatter: '{value}%',
						textStyle: {
							color: "#adadad"
						}
					},
					max: 100,
					axisTick: {
						show: false
					},
					axisLine: {
						lineStyle: {
							color: "#e6e6e6",
							type: 'solid',
							opacity: 0.5
						}
					},
					splitLine: {
						show: true,
						lineStyle: {
							type: 'solid',
							color: "#f0f0f0"
						}
					}
				}],
				series: [{
					name: '训练活跃度',
					type: 'line',
					symbolSize: 6,
					lineStyle: {
						normal: {
							color: "#ff7247",
							width: 2
						}
					},
					smooth: that.smooth,
					stack: '总量',
					itemStyle:{normal:{borderColor:"#ff7247",borderWidth:2,opacity:1}} ,
					label: {
						normal: {
							show: false,
							position: 'top'
						}
					},
					areaStyle: {
						normal: {
							color: "#ff4c23"
						}
					},
					data: that.data
				}]
			}

			return opt;
		},
		arr_eg: function(arr) {
			for (var i = 0; i < arr.length; i++) {
				var day = new Date(arr[i]);
				arr[i] = day.getFullYear() + "年" + (day.getMonth() + 1) + "月" + day.getDate() + "日";
			}
			arr[0] = {
				'value': arr[0],
				'textStyle': {
					'align': 'left'
				}
			};
			arr[arr.length - 1] = {
				'value': arr[arr.length - 1],
				'textStyle': {
					'align': 'right'
				}
			};
            if(arr.length==1){
                arr=["暂无数据"]
            }
             console.log(arr.length)
			return arr;
		},
		format: function (a,n){
			function band(i,r){
				if(i%(a) == 0) {
					return r;
				}
				if(i == n){
					return r;
				}
			};
			return band;
		},
		get_ajax: function (class_id,days){
			var that=this;
			$.post("http://121.41.47.79:3000/get_daily_training_rate",{"class_id":class_id,"days":days},function(result){
				if (result.header.code == 200) {
					that.key_arr=[]; that.value_arr=[];
					 for(var i in result.data.training_rate){
					 	that.key_arr[i]=result.data.training_rate[i].ds;
					 	that.value_arr[i]=result.data.training_rate[i].rate*100;
					 }
					that.chart = echarts.init(that.ele);
					that.data = that.value_arr;
					if(that.data.length > '8'){
						that.a = parseInt(that.data.length/4);
					}else{
						that.a = 1;
					};
			        that.n = that.data.length-1;
			        that.fsyle = that.format(that.a,that.n);
			        that.arr_x = that.arr_eg(that.key_arr);
			        that.chart.setOption(that.ops());
				}else {
                    new Notice(result.header.msg)
					//alert(result.header.msg)
				}
			})
		},
		something: function(){
			var that=this;
            $(".select_group div").each(function(index){
                return function(){
                    if($($(".select_group div")[index]).hasClass("over")){
                        switch (index){
                            case 0:
                                that.days_one();
                                break;
                            case 1:
                                that.days_two();
                                break;
                            case 2:
                                that.days_three();
                                break;
                            case 3:
                                that.days_four();
                                break;
                            case 4:
                                that.days_five();
                                break;
                            default:
                                new Notice("数据出错，请联系管理员！")
                               // alert("数据出错，请联系管理员！")
                                break;
                        }
                    }
                }(index)
            })
			$("div",".select_group").each(function(i,r){

					$(this).click(function(){
						$(this).removeClass("def").addClass("over").siblings("div").removeClass("over").addClass("def");
						switch (i){
							case 0:
							    that.days_one();
								break;
							case 1:
							    that.days_two();
								break;
							case 2:
							    that.days_three();
								break;
							case 3:
							    that.days_four();
								break;
							case 4:
							    that.days_five();
								break;
							default:
                                new Notice("数据出错，请联系管理员！")
							   // alert("数据出错，请联系管理员！")
								break;
						}
					})
				})
		},
        getClassAndGrade:function(){

        },
		days_one: function(){
			this.days= 7;
			this.get_ajax(this.class_id,this.days);
			this.sbool = true;
			this.smooth = false;
            setInterval(function(){
                if("undefined%" == $(".day_ty").html()){
                    $(".day_ty").html("暂无数据")
                }
            },100)

		},
		days_two: function(){
			this.days=30;
			this.get_ajax(this.class_id,this.days);
			this.sbool = true;
			this.smooth = true;
		},
		days_three: function(){
			this.days= 90;
			this.get_ajax(this.class_id,this.days);
			this.sbool = true;
			this.smooth = true;
		},
		days_four: function(){
			this.days= 180;
			this.get_ajax(this.class_id,this.days);
			this.sbool = true;
			this.smooth = true;
		},
		days_five: function(){
			this.days= 365;
			this.get_ajax(this.class_id,this.days);
			this.sbool = true;
			this.smooth = true;
		}

	}

}