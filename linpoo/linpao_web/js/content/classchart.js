/*@author By lizhihu
 * @date By 2016-01-23
 */
function hideList(){
	 $(".inputList").slideUp(500);
}
var PersonHealth = function(){
  this.term = "";
  this.year = "";
  this.grade = "";
  this.classRoom = "";
  this.sex= "";
    this.productName=""
  $.easing.def = "easeInBack";

}
var gradeNameModel = [];
var classNameModel = [];
PersonHealth.prototype.getPersonListYear = function(){
	$("#choiceYear").html($($("#yearListId .list div")[0]).context.innerText);
	//学年
	$("#yearId").on("click",function(e){
		 hideList();
		if($("#yearListId").css("display")!="block"){
			$("#yearListId").slideDown(500);
		}else{
			$("#yearListId").slideUp(500);
		}
        stopBubble(e)
		//event.stopPropagation();
	});
	$("#yearListId .list div").each(function(index,val){
		return function(){
			$($("#yearListId .list div")[index]).click(function(){
				var _this = this;
				$("#choiceYear").html($(_this).context.innerText);
				$("#yearListId").slideUp(500);
			})
		}(index)
		
	})
	//点击现在
	$("#nowYear").click(function(){
		$("#choiceYear").html($($("#yearListId .list div")[0]).context.innerText);
		$("#yearListId").slideUp(500);
	})
	$(document).click(function(event){
		$("#yearListId").slideUp(500);
	})
};

PersonHealth.prototype.getPersonListGrade = function() {

    $("#choiceGrade").html($($("#gradeListId .list div")[0]).context.innerText);
    $("#gradeId").on("click", function(e) {
        hideList();
        if ($("#gradeListId").css("display") != "block") {
            $("#gradeListId").slideDown(500);
            $("#gradeId img").attr("src", "img/moreup_gray.png");
        } else {
            $("#gradeListId").slideUp(500);
            $("#gradeId img").attr("src", "img/moredown_gray.png");
        }
        stopBubble(e)
        //event.stopPropagation();
    })
    //选择学期
    $("#gradeListId .list div").each(function(index, val) {
        return function() {
            $($("#gradeListId .list div")[index]).click(function() {
                var _this = this;
                $("#choiceGrade").html($(_this).context.innerText);
                $("#gradeListId").slideUp(500);
                $("#gradeId img").attr("src", "img/moredown_gray.png");
                var _this = this;
                $("#choiceGrade").html($(_this).context.innerText);

                $("#gradeListId").slideUp(500);
                $("#gradeId img").attr("src", "img/moredown_gray.png");

                //初始化另一个
                for (var i = 0; i < gradeNameModel.length; i++) {
                    if ($("#choiceGrade").text() == gradeNameModel[i]) {
                        classIdNum = i;
                    }
                }
                var classHtml = "";
                for (var j = 0; j < classNameModel[classIdNum].length; j++) {
                    classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
                }
                $("#classListHtmlId").html(classHtml);
                $("#choiceClass").html($($("#classListHtmlId div")[0]).text());

            })
        }(index)

    })
    $(document).click(function(event) {
        $("#gradeListId").slideUp(500);
        $("#gradeId img").attr("src", "img/moredown_gray.png");
    })
}
PersonHealth.prototype.getPersonListClass = function() {
    $("#choiceClass").html($($("#classListId .list div")[0]).context.innerText);
    $("#classId").on("click", function(e) {
        hideList();
        if ($("#classListId").css("display") != "block") {
            $("#classListId").slideDown(500);
            $("#classId img").attr("src", "img/moreup_gray.png");
        } else {
            $("#classListId").slideUp(500);
            $("#classId img").attr("src", "img/moredown_gray.png");
        }
        stopBubble(e)
        //event.stopPropagation();
        $("#classListId .list div").each(function(index, val) {
            return function() {
                $($("#classListId .list div")[index]).click(function() {
                    var _this = this;
                    $("#choiceClass").html($(_this).context.innerText);
                    $("#classListId").slideUp(500);
                    $("#classId img").attr("src", "img/moredown_gray.png");
                })
            }(index)

        })
    })
    //选择学期

    $(document).click(function(event) {
        $("#classListId").slideUp(500);
        $("#classId img").attr("src", "img/moredown_gray.png");
    })
}
//项目
PersonHealth.prototype.getPersonListProduct = function(){
	$("#choiceProduct").html($($("#productListId .list div")[0]).context.innerText);
	$("#productId").on("click",function(e){
		 hideList();
		if($("#productListId").css("display")!="block"){
			$("#productListId").slideDown(500);
			 $("#productId img").attr("src","img/moreup_gray.png");
		}else{
			$("#productListId").slideUp(500);
			$("#productId img").attr("src","img/moredown_gray.png");
		}
        stopBubble(e)
		//event.stopPropagation();
	})
	//选择项目
	$("#productListId .list div").each(function(index,val){
		return function(){
			$($("#productListId .list div")[index]).click(function(){
				var _this = this;
				$("#choiceProduct").html($(_this).context.innerText);
				$("#productListId").slideUp(500);
				$("#productId img").attr("src","img/moredown_gray.png");
			})
		}(index)
		
	})
	$(document).click(function(event){
		$("#productListId").slideUp(500);
		$("#productId img").attr("src","img/moredown_gray.png");
	})
}
PersonHealth.prototype.bindEvent = function(){
	var claerTime,claerTime1;
	$(".biaoNow").hover(function(){
		clearTimeout(claerTime1);
		clearTimeout(claerTime);
		if($(".biaoList").css("display")!="block"){
			$(".biaoList").slideDown('500');
			$($(".biaoNow").find("img")[0]).attr("src","img/moreup_gray.png");
		}
	},function(event){
		claerTime =  setTimeout(function(){
			$(".biaoList").slideUp('500');
			$($(".biaoNow").find("img")[0]).attr("src","img/moredown_gray.png");
		},500)
		
	});
	$(".biaoList").hover(function(){
		clearTimeout(claerTime);
		clearTimeout(claerTime1);
		$(".biaoList").css("display","block");
		},function(){
			claerTime1 =  setTimeout(function(){
				$(".biaoList").slideUp('500');
				$($(".biaoNow").find("img")[0]).attr("src","img/moredown_gray.png");
			},500)
		})

}
function getTableData(data){
   var tableLevalHtml = "";
    var allNum = 0;
    var dataArrAll = data.data.class_level_chart;
    var grateBoy = dataArrAll.boy_great;
    var grateGirl = dataArrAll.girl_great;
    var goodBoy = dataArrAll.boy_good;
    var goodGirl = dataArrAll.girl_good;
    var normalBoy = dataArrAll.boy_normal;
    var normalGirl = dataArrAll.girl_normal;
    var failedBoy = dataArrAll.boy_failed;
    var failedGirl = dataArrAll.girl_failed;
    console.log("goodBoy:",goodBoy)
    if(grateBoy.length>0){
        for(var i=0;i<grateBoy.length;i++){
            tableLevalHtml+=grateBoy[i]+"  ";
            $("#grateBoyNum").html(i+1+"个人");
        }
        $("#grateBoy").html(tableLevalHtml);
    }else{
        $("#grateBoy").html("");
        $("#grateBoyNum").html(0+"个人");
    }
    tableLevalHtml = "";
    if(grateGirl.length>0){
        for(var i=0;i<grateGirl.length;i++){
            tableLevalHtml+=grateGirl[i]+"  ";
            $("#grateGirlNum").html(i+1+"个人");
        }
        $("#grateGirl").html(tableLevalHtml);

    }else{
        $("#grateGirl").html("");
        $("#grateGirlNum").html(0+"个人");
    }
    tableLevalHtml = "";
    if(goodBoy.length>0){
        for(var i=0;i<goodBoy.length;i++){
            tableLevalHtml+=goodBoy[i]+"  ";
            $("#goodBoyNum").html(i+1+"个人");
        }
        $("#goodBoy").html(tableLevalHtml);
    }else{
        $("#goodBoy").html("");
        $("#goodBoyNum").html(0+"个人");
    }
    tableLevalHtml = "";
    if(goodGirl.length>0){
        for(var i=0;i<goodGirl.length;i++){
            tableLevalHtml+=goodGirl[i]+"  ";
            $("#goodGirlNum").html(i+1+"个人");
        }
        $("#goodGirl").html(tableLevalHtml);
    }else{
        $("#goodGirl").html("");
        $("#goodGirlNum").html(0+"个人");
    }
    tableLevalHtml = "";
    if(normalBoy.length>0){
        for(var i=0;i<normalBoy.length;i++){
            tableLevalHtml+=normalBoy[i]+"  ";
            $("#normalBoyNum").html(i+1+"个人");
        }
        $("#normalBoy").html(tableLevalHtml);
    }else{
        $("#normalBoy").html("");
        $("#normalBoyNum").html(0+"个人");
    }
    tableLevalHtml = "";
    if(normalGirl.length>0){
        for(var i=0;i<normalGirl.length;i++){
            tableLevalHtml+=normalGirl[i]+"  ";
            $("#normalGirlNum").html(i+1+"个人");
        }
        $("#normalGirl").html(tableLevalHtml);
    }else{
        $("#normalGirl").html("");
        $("#normalGirlNum").html(0+"个人");
    }
    tableLevalHtml = "";
    if(failedBoy.length>0){
        for(var i=0;i<failedBoy.length;i++){
            tableLevalHtml+=failedBoy[i]+"  ";
            $("#fialedBoyNum").html(i+1+"个人");
        }
        $("#fialedBoy").html(tableLevalHtml);
    }else{
        $("#fialedBoy").html("");
        $("#fialedBoyNum").html(0+"个人");
    }
    tableLevalHtml = "";
    if(failedGirl.length>0){
        for(var i=0;i<failedGirl.length;i++){
            tableLevalHtml+=failedGirl[i]+"  ";
            $("#failedGirlNum").html(i+1+"个人");
        }
        $("#failedGirl").html(tableLevalHtml);
    }else{
        $("#failedGirl").html("");
        $("#failedGirlNum").html(0+"个人");
    }

}
PersonHealth.prototype.defalutData = function(){
	      this.year = $("#choiceYear").text();
		  this.grade = $("#choiceGrade").text();
		  this.classRoom = $("#choiceClass").text();
          this.productName = $("#choiceProduct").text();
          var product = productArr[this.productName];
          var year = this.year;
          var className = this.grade+","+this.classRoom;
          className = dataBrr[className];
           //2016 项目1 1102有数据
		  	$("#title").html("测试合作学校"+this.year+"学年"+this.grade+this.classRoom+this.productName+"项目健康指标数据统计");
           var data1 = {
            "year": year,
            "item_id":product,
            "class_id":className
           };

            $.ajax({
                data: data1,
                type: "post",
                url: getURL()+"class_level_chart",
                success: function(dataRes) {
                    console.log(dataRes)
                    if(dataRes.header.code=="200"){
                        getTableData(dataRes)
                    }else{
                        new Notice(dataRes.header.msg)
                        //alert(dataRes.header.msg)
                    }

                }
            })
		  	 /*var url = getURL();
		     var data = {}
		     $.ajax({
		         data:data,
		         type : "post", 
		         url : url,
		         success : function(dataRes){
		         	
		         }
            });*/
}
//班级分割
function classAndGrade(data) {
    var arrGradeAndClass = new Array();
    var commonData = dataArr;
    var temp = ""
    var gradeHtml = "";
    var classHtml = "";
    var classIdNum;
    for (var i = 0; i < data.length; i++) {
        var gradeName = commonData[data[i]].split(",")[0];
        var className = commonData[data[i]].split(",")[1];
        if (temp != gradeName) {
            gradeNameModel.push(gradeName);
            temp = gradeName;

        }
    }
    gradeSort(gradeNameModel)
    for (var j = 0; j < gradeNameModel.length; j++) {
        gradeHtml += '<div><span>' + gradeNameModel[j] + '</span></div>'
        $("#gradeListHtmlId").html(gradeHtml);
        $("#choiceGrade").html($($("#gradeListHtmlId div")[0]).text());
    }
    for (var k = 0; k < gradeNameModel.length; k++) {
        classNameModel[k] = new Array();
        for (var j = 0; j < data.length; j++) {
            if (gradeNameModel[k] == commonData[data[j]].split(",")[0]) {

                classNameModel[k].push(commonData[data[j]].split(",")[1])
            }
        }
        classSort(classNameModel[k])
    }

    for (var i = 0; i < gradeNameModel.length; i++) {
        if ($("#choiceGrade").text() == gradeNameModel[i]) {
            classIdNum = i;
        }
    }
    var classHtml = "";
    for (var j = 0; j < classNameModel[classIdNum].length; j++) {
        classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
    }
    $("#classListHtmlId").html(classHtml);
    console.log(classHtml)
    $("#choiceClass").html($($("#classListHtmlId div")[0]).text());
    defalutClassText = $($("#classListHtmlId div")[0]).text();
}

PersonHealth.prototype.getAllData = function(){
	
	var _this = this;
	$("#charCheck").on("click",function(){

		_this.defalutData();
	})
	
}
$(document).ready(function(){
    var school=localStorage.getItem("schoolName");
    var name = localStorage.getItem("userName")
    var is_root = localStorage.getItem("is_root")
    var dataSchoolInfo ={"name":name,"schoolName":school,"is_root":is_root};
    getStuInfo(dataSchoolInfo)
    var url = getURL() + "get_user_class";
    var data = {
        "account": name
    };
    $.ajax({
        data: data,
        type: "post",
        url: url,
        success: function (dataRes) {
            console.log("classChart:",dataRes)
            if(dataRes.header.code=="200"){
                if(dataRes.data.user_class!=0){
                    classAndGrade(dataRes.data.user_class);
                }else{
                    new Notice("数据获取失败")
                    //alert("数据获取失败")
                }
                //初始化数据
                var dataHeal = new PersonHealth();
                dataHeal.bindEvent();
                //学年
                dataHeal.getPersonListYear();
                //学期
                dataHeal.getPersonListGrade();
                //班级
                dataHeal.getPersonListClass();
                //项目
                dataHeal.getPersonListProduct();
                //获取默认值
                dataHeal.defalutData();
                //点击后数据展示
                dataHeal.getAllData();
            }else{
                new Notice(dataRes.header.msg)
                //alert(dataRes.header.msg)
            }



        }
    });
})
