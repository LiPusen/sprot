/**
 * Created by hard work on 2016/1/31.
 */
var FileUp = function(){
    $.easing.def = "easeInBack";
}
function hideList() {
    $(".t_drop").slideUp(500);
}
FileUp.prototype.getYear = function(){
    //学年
    $("#yearId").on("click", function(e) {
        $("#inputScore").hide();
        hideList();
        if ($("#yearListId").css("display") != "block") {
            $("#yearListId").slideDown(500);
            $("#yearId img").attr("src", "img/moreup_gray.png");
        } else {
            $("#yearListId").slideUp(500);
            $("#yearId img").attr("src", "img/moredown_gray.png");
        }
        stopBubble(e)
        //event.stopPropagation();
    });
    $("#yearListId .t_list div").each(function(index, val) {
        return function() {
            $($("#yearListId .t_list div")[index]).click(function() {
                var _this = this;
                $("#choiceYear").html($(_this).context.innerText);
                $("#yearListId").slideUp(500);
            })
        }(index)

    })
    //点击现在
    $("#nowYear").click(function() {
        $("#choiceYear").html($($("#yearListId .t_list div")[0]).context.innerText);
        $("#yearListId").slideUp(500);
        $("#yearId img").attr("src", "img/moredown_gray.png");
    })
    $(document).click(function(event) {
        $("#yearListId").slideUp(500);
        $("#yearId img").attr("src", "img/moredown_gray.png");
    })

}
FileUp.prototype.getTerm = function(){
    $("#termIdFile").on("click", function(e) {
        $("#inputScore").hide();
        hideList();
        if ($("#termListIdFIle").css("display") != "block") {
            $("#termListIdFIle").slideDown(500);
            $("#termIdFile img").attr("src", "img/moreup_gray.png");
        } else {
            $("#termListIdFIle").slideUp(500);
            $("#termIdFile img").attr("src", "img/moredown_gray.png");
        }
        stopBubble(e)
        //event.stopPropagation();
    })
    //选择学期
    $("#termListIdFIle .t_list div").each(function(index, val) {
        return function() {
            $($("#termListIdFIle .t_list div")[index]).click(function() {
                var _this = this;
                $("#choiceTermList").html($(_this).context.innerText);

                $("#termListIdFIle").slideUp(500);
                $("#termIdFile img").attr("src", "img/moredown_gray.png");
            })
        }(index)

    })
    $(document).click(function(event) {
        $("#termListIdFIle").slideUp(500);
        $("#termIdFile img").attr("src", "img/moredown_gray.png");
    })
}
FileUp.prototype.getOutYear = function(){
    //学年
    $("#schoolYearId").on("click", function(e) {
        hideList();
        if ($("#schoolYearListId").css("display") != "block") {
            $("#schoolYearListId").slideDown(500);
            $("#schoolYearId img").attr("src", "img/moreup_gray.png");
        } else {
            $("#schoolYearListId").slideUp(500);
            $("#schoolYearId img").attr("src", "img/moredown_gray.png");
        }
        stopBubble(e)
        //event.stopPropagation();
    });
    $("#schoolYearListId .t_list div").each(function(index, val) {
        return function() {
            $($("#schoolYearListId .t_list div")[index]).click(function() {
                var _this = this;
                $("#choiceSchoolYear").html($(_this).context.innerText);
                $("#schoolYearListId").slideUp(500);
            })
        }(index)

    })
    //点击现在
    $("#nowSchoolYear").click(function() {
        $("#choiceSchoolYear").html($($("#yearListId .t_list div")[0]).context.innerText);
        $("#schoolYearListId").slideUp(500);
        $("#schoolYearId img").attr("src", "img/moredown_gray.png");
    })
    $(document).click(function(event) {
        $("#schoolYearListId").slideUp(500);
        $("#schoolYearId img").attr("src", "img/moredown_gray.png");
    })

}
FileUp.prototype.getOutTerm = function(){
    $("#schoolTermId").on("click", function(e) {
        hideList();
        if ($("#schoolTermListId").css("display") != "block") {
            $("#schoolTermListId").slideDown(500);
            $("#schoolTermId img").attr("src", "img/moreup_gray.png");
        } else {
            $("#termListIdFIle").slideUp(500);
            $("#schoolTermId img").attr("src", "img/moredown_gray.png");
        }
        stopBubble(e)
        //event.stopPropagation();
    })
    //选择学期
    $("#schoolTermListId .t_list div").each(function(index, val) {
        return function() {
            $($("#schoolTermListId .t_list div")[index]).click(function() {
                var _this = this;
                $("#choiceSchoolTerm").html($(_this).context.innerText);

                $("#schoolTermListId").slideUp(500);
                $("#schoolTermId img").attr("src", "img/moredown_gray.png");
            })
        }(index)

    })
    $(document).click(function(event) {
        $("#schoolTermListId").slideUp(500);
        $("#schoolTermId img").attr("src", "img/moredown_gray.png");
    })
}
FileUp.prototype.upLoad = function(){
    var inputChange = $(".download-btn-box input");
    inputChange.change(function(){
        var _this = this;
        var schoolNameScore = $(_this).val();
        var arr = schoolNameScore.split('\\');
        schoolNameScore=arr[arr.length-1];//这就是要取得的图片名称
        var htmlUpload="";
        if($.trim($("#file_upload").val())!=""){
            $("#inputScore").hide();
            htmlUpload+= '<li><span class="ico"></span><span class="file-name">'+schoolNameScore+'</span> </li>';
            $("#upload").html(htmlUpload)
        }else{
            $("#upload").html("");
            $("#inputScore").show();
            $("#inputScore").html("*您取消了文件选择，请重新选择上传文件");
        }
        /*$("#upload li").each(function(index,value){
            return function(){
               $($("#upload li")[index]).find("a").on("click",function(){
                   alert("删除成功")
               })
            }(index)
        })*/
    })

}
var bartimer;
function setProcess(){
    var processbar = document.getElementById("progressBar");
    processbar.style.width = parseInt(processbar.style.width) + 1 + "%";
    $("#progressBar").css("width",processbar.style.width);
    $("#progressLen").text(processbar.style.width);
    if(processbar.style.width == "100%"){
        window.clearInterval(bartimer);
       // new Notice("上传成功");
        $("#upload").html("")
        $("#inputScore").show();
        $("#inputScore").html("文件上传成功");
        setTimeout(function(){
            $("#procressId").hide();
            processbar.style.width="0%";

        },100)
        setTimeout(function(){
            $("#upload").html("")
            $("#inputScore").hide();
        },1000)

    }
}

$(document).ready(function(){
    //console.log($(".download-btn-box input")[0])
    var school=localStorage.getItem("schoolName");
    var name = localStorage.getItem("userName")
    var is_root = localStorage.getItem("is_root");
    var schoolIdName = localStorage.getItem("schoolId");
    var dataSchoolInfo ={"name":name,"schoolName":school,"is_root":is_root};
    getStuInfo(dataSchoolInfo)
    var fileDown = new FileUp();
    fileDown.getYear();
    fileDown.getTerm();
    fileDown.upLoad();
    function doUpload() {
        var termList = $.trim($("#choiceTermList").text());
        termList  = dataTerm[termList];
        $("#year").val($.trim($("#choiceYear").text()));
        $("#term").val(termList)
        $("#school").val(school)
        $("#school_id").val(schoolIdName);
        $("#account").val(name);

        var formData = new FormData($( "#uploadForm" )[0]);
       $.ajax({
            url: getURL()+"score_input" ,
            type: 'post',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (returndata) {
               if(returndata.header.code=="200"){
                   if(returndata.data.result=="0"){
                       console.log(returndata)

                   }
               }
            },
            error: function (returndata) {
                console.log(returndata);
            }
        });
    }
    $(".sub").on("click",function(){
        if($("#choiceYear").text()=="请选择"){
            $("#inputScore").show();
            $("#inputScore").html("*请选择学年");
            return;
            //alert("请选择学年");
        }else if($("#choiceTermList").text()=="请选择"){
            $("#inputScore").show();
            $("#inputScore").html("*请选择学期");
            return;
           //alert("请选择学期")
        }else{
            $("#inputScore").hide();
            if($.trim($("#upload").html())){
                $("#procressId").css("display","block");
                bartimer = setInterval(function(){
                    setProcess();
                },100);
                doUpload();
                return;
            }else{
                $("#inputScore").show();
                $("#inputScore").html("*请选择文件");
                return;
                //alert("请选择文件");
            }

        }

    })
    fileDown.getOutYear();
    fileDown.getOutTerm();
    $("#scoueOut").on("click",function() {
        if ($("#choiceSchoolYear").text() == "请选择") {
            alert("请选择学年");
        } else if ($("#choiceSchoolTerm").text() == "请选择") {
            alert("请选择学期")
        } else {

            var url = getURL() + "score_output";
            var schoolClassTerm = $("#choiceSchoolTerm").text();

            schoolClassTerm = dataTerm[$.trim(schoolClassTerm)];
            console.log(schoolClassTerm)
            var data = {"class_id": "", "school_id": "1", "year": $.trim($("#choiceSchoolYear").text()), "term": schoolClassTerm};

            $.ajax({
                data: data,
                type: "post",
                url: url,
                success: function (dataRes) {
                    console.log("hello:", dataRes)
                    if (dataRes.header.code == "200") {
                        if(dataRes.data.result!=-1){
                            window.location.href=dataRes.data.url;
                        }else{
                           /* $("#procressId").css("display","block");
                             //setInterval(function(){
                            for(var i=0;i<100;i++){
                                $("#progressBar").css("width",i+"%");
                            }*/

                             //},2000)
                           // alert(dataRes.data.msg);
                        }

                    } else {
                        alert(dataRes.header.msg)
                    }
                }

            })
        }
    });
})