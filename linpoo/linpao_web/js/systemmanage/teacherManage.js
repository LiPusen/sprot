/**
 * Created by hard work on 2016/2/2.
 */
var TeacherManage = function(){

};
var gradeNameModelTep= [];
var classNameModelTep = [];
var htmlConnect="";
function hideList() {
    $(".t_drop").slideUp(500);
}
var gradeIndex = new Array();
TeacherManage.prototype.getTeacherAcco = function(){
    var _this = this;
    var schoolIdName = localStorage.getItem("schoolId");
    var data = {
        "school_id":schoolIdName
    }
    $.ajax({
        url: getURL()+"get_teacher" ,
        type: 'post',
        data: data,
        success: function (returndata) {
           if(returndata.header.code=="200"){
               console.log("returndata::",returndata)
               var teacherData = returndata.data.teacher_list;
               var html="";
               for(var i=0;i<teacherData.length;i++){
                   var classList = [];
                   var classListInit = teacherData[i].class_list.split(",");
                   var str="";
                   for(var k=0;k<classListInit.length;k++){
                       if(k==classListInit.length-1){
                           str += dataArr[classListInit[k]]
                       }else{
                           str += dataArr[classListInit[k]]+"/";
                       }

                   }
                   html+='<div class="col-lg-6 items"><ul> <li><span class="names">'+teacherData[i].teacher_name+'</span></li>';
                   html+='<li><span class="phone-lab">账号:</span><span>'+teacherData[i].account+'</span></li>';
                   html+='<li><span class="lab">班级:</span><p class="inline t_s">'+str+'</p</li></ul>';
                   html+='<div class="toolbar"> <a class="change ">修改</a>  <a class="del" id="'+teacherData[i].id+'">删除</a></div></div>';
               }
               $("#teachId").html(html);
               _this.deleteTea();
               _this.modelTeacher();
               modTeachInfo()
               $(".t_s").mCustomScrollbar({
					setHeight:110,
					theme:"dark-2"
				});
           }else{
               alert(returndata.header.msg);
           }
        },
        error: function (returndata) {
            alert(returndata);
        }
    });
}
//设置年级
function setGrade(dataGrade){
    var gradeHtml = ""
    //click-active
    for (var k = 0; k < dataGrade.length; k++) {
        gradeHtml += '<li class="" id="'+k+'grade"><span class="">' + dataGrade[k] + '</span></li>'
    }
    $("#allgrade").html(gradeHtml);
    $($("#allgrade").find("li")[0]).attr("class","click-active");

}
//设置班级
function setClass(dataClass){
 //默认初始化班级
    var classHtml=""
    for (var i = 0; i < dataClass[0].length; i++) {
        classHtml += '<li>' + dataClass[0][i] + ' <span class="checkbox-ico-uncheck"></span></li>'
    }
    $("#fallowClass").html(classHtml);
}

function choiceGrade(){
    var gradeLen = $("#allgrade").find("li").length;
    for(var i=0;i<gradeLen;i++){
        gradeIndex.push(i+"index");
    }
    var indexNum=0;
    choiceClass(indexNum);
    $("#allgrade").find("li").each(function(index){
           return function(){
              $( $("#allgrade").find("li")[index]).on("click",function(){
                  indexNum=index;
                  var classData = classNameModelTep[index];
                  var classHtml=""
                      for(var i=0;i<classData.length;i++){
                          classHtml += '<li>' + classData[i] + ' <span class="checkbox-ico-uncheck"></span></li>'
                      }
                  $("#fallowClass").html(classHtml);
                  $("#allgrade").find("li").attr("class","");
                  $($("#allgrade").find("li")[index]).attr("class","click-active");
                  choiceClass(indexNum);
                  $("#connectClass").find("label").each(function(classNum){
                      return function(){
                          if( $($("#connectClass").find("label")[classNum]).text().indexOf(($($("#allgrade").find("li")[index]).text()))!=-1){
                               var strClass = $($("#connectClass").find("label")[classNum]).text().substring(3,5)
                              $("#fallowClass").find("li").find("span").each(function(indexClass){
                                    return function(){
                                          if($.trim($($("#fallowClass").find("li")[indexClass]).text()) == $.trim(strClass)){
                                              $($("#fallowClass").find("li").find("span")[indexClass]).attr("class","checkbox-ico");
                                          }
                                    }(indexClass)
                              });
                          }
                      }(classNum)
                  })

              })
           }(index);
    })

}

function choiceClass(indexNum){

    $("#fallowClass").find("li").find("span").each(function(index1){
        return function(){
            $($("#fallowClass").find("li").find("span")[index1]).on("click",function(){
                if($($("#fallowClass").find("li").find("span")[index1]).hasClass("checkbox-ico-uncheck")){
                    $($("#fallowClass").find("li").find("span")[index1]).attr("class","checkbox-ico");
                    htmlConnect += '<label class="class-item">' + gradeNameModelTep[indexNum] + classNameModelTep[indexNum][index1] + '<span class=""></span></label>'
                    $("#connectClass").html(htmlConnect);

                    //delImg();
                }else{
                    $($("#fallowClass").find("li").find("span")[index1]).attr("class","checkbox-ico-uncheck");
                  $("#connectClass").find("label").each(function(index3,value){
                        return function(){
                            if($($("#connectClass").find("label")[index3]).text()==(gradeNameModelTep[indexNum] + classNameModelTep[indexNum][index1])){
                                $($("#connectClass").find("label")[index3]).remove();
                                //实时跟新删除的代码
                                htmlConnect = $("#connectClass").html()

                            }
                        }(index3)
                    })
                }
                //找到对应的班级
                findClassName()
               // $($("#fallowClass").find("li").find("span")[index1]).attr("class","checkbox-ico")
            })
        }(index1)
    })

}
var labelName = []
var labelNameMod=[]
function findClassName(){

    $("#connectClass").find("label").each(function(index3,value){
         labelName[index3]=( $($("#connectClass").find("label")[index3]).text())
    })
    $("#allgrade").find("li").each(function(index) {
       for(var i=0;i<labelName.length;i++){
            if($($("#allgrade").find("li")[index]).hasClass("click-active")){
               for(var k=0;k<$("#fallowClass").find("li").find("span").length;k++){
                   var className = $($("#fallowClass").find("li")[k]).text();
                   var gradeName = $($("#allgrade").find("li")[index]).text()
                   var classAndGrade = gradeName+className;
                   if($.trim(labelName[i])==($.trim(classAndGrade))){
                       $($("#fallowClass").find("li").find("span")[k]).attr("class","checkbox-ico");
                   }
               }

            }
       }

    })
    labelName=[];
}
TeacherManage.prototype.addTeacher = function(){
    $("#addAccountImg").on("click", function() {
        $("#accountAlter").css("display", "block");
        $("#popId").show();
        $("#popId").html(showPop());
    });

    //下拉框
    $("#openList").on("click", function() {
        if ($("#openList").hasClass("ico")) {
            $("#openList").attr("class", "ico-up");
            $("#TeacherclassListDetail").slideDown(100);
        } else {
            $("#openList").attr("class", "ico");
            $("#TeacherclassListDetail").slideUp(100);
        }
        //下拉以后的显示内容
    })
    setGrade(gradeNameModelTep);
    //设置班级
    setClass(classNameModelTep);
    //点击每个年级
    choiceGrade();
    //选中每个班级
}
//删除账号
function deleteTeacher(id){
    var url = getURL() + "del_teacher";
    new  ModelCon("确认删除");
    $(".isSure").off().on("click",function(){
        $.ajax({
            data: {id:id},
            type: "post",
            url: url,
            success:function(data){
                if(data.data.result=="0"){

                    $(".mod_wapper").animate({"height" : 0},300,function() {
                        $(".markHide").fadeOut(1);
                    })
                    setTimeout(function(){
                        new Notice(data.data.msg);
                        window.location.href="systemmanage.html";
                    },300)
                }else{
                    $(".mod_wapper").animate({"height" : 0},300,function() {
                        $(".markHide").fadeOut(1);
                    })
                    setTimeout(function(){
                        new Notice(data.data.msg);
                        window.location.href="systemmanage.html";
                    },300)
                }
            },error:function(){
                $(".mod_wapper").animate({"height" : 0},300,function() {
                    $(".markHide").fadeOut(1);
                })
                new Notice("删除失败");
            }
        });
    });
    $(".isCancleOk").off().on("click",function(){
        $(".mod_wapper").animate({"height" : 0},300,function() {
            $(".markHide").fadeOut(1);
        })
    });
    $(".mod_quit").off().on("click",function(){
        $(".mod_wapper").animate({"height" : 0},300,function() {
            $(".markHide").fadeOut(1);
        })
    });
}
//删除账号
TeacherManage.prototype.deleteTea = function(){
    $("#teachId").find("div .toolbar").find(".del").each(function(index){
        return function(){
            $( $("#teachId").find("div .toolbar").find(".del")[index]).on("click",function(){
               var _this = this;
                deleteTeacher($(_this).attr("id"));
                //
            })
        }(index)
    })

}
//修改账号
TeacherManage.prototype.modelTeacher = function(){
    $("#teachId").find("div .toolbar").find(".change ").each(function(index){
        return function(){
            $( $("#teachId").find("div .toolbar").find(".change")[index]).on("click",function(){
               //teacher_name teacher_phone class_list
                $("#accountMod").css("display", "block");
                $("#popId").show();
                $("#popId").html(showPop());
                var _this = this;
                var modName = $($("#teachId").find("div ul")[index]).find(".names").text();
                var modPhone = $($("#teachId").find("div ul")[index]).find(".phone-lab").next().text();
                var modClass = $($("#teachId").find("div ul")[index]).find(".lab").next().text();
               var modHtmlCon = ""
                var modeClassArr=[];
               $($("#accountMod").find("ul li")[0]).find("input").val(modName)
                $($("#accountMod").find("ul li")[1]).find("input").val(modPhone);
                modClass = modClass.split("/");
                for(var i=0;i<modClass.length;i++){
                    modeClassArr[i] = modClass[i].split(",").join("");
                }
                for(var j=0;j<modeClassArr.length;j++){
                    modHtmlCon += '<label class="class-item">' + modeClassArr[j] + '<span></span></label>'

                }
                $("#connectClassMod").html(modHtmlCon);
            })
        }(index)
    })
}
function addTeaInfo(msg){
    $("#addTeaInfo").show();
    $("#addTeaInfo").html(msg);
}
function addTeaInfoModel(msg){
    $("#addTeaInfoModel").show();
    $("#addTeaInfoModel").html(msg);
}
//点击菜单下拉修改
function modTeachInfo(){
    $("#openListMod").on("click", function() {
        if ($("#openListMod").hasClass("ico")) {
            $("#openListMod").attr("class", "ico-up");
            $("#TeacherclassListDetailMod").slideDown(100);
        } else {
            $("#openListMod").attr("class", "ico");
            $("#TeacherclassListDetailMod").slideUp(100);
        }
        setGradeMod(gradeNameModelTep);
        setClassMod(classNameModelTep);
        //点击每个年级
        choiceGradeMod();
        //选中每个班级
        findClassNameMod()

    })
    $("#addTeacherIdMod").on("click",function(){
        var teachName = $($("#accountMod").find("ul li")[0]).find("input").val();
        var teachPhone = $($("#accountMod").find("ul li")[1]).find("input").val();
        var labelNameMod = new Array();
        $("#connectClassMod").find("label").each(function(index){
            labelNameMod[index]=( $($("#connectClassMod").find("label")[index]).text())
        })
        var labelNameList = "";
        for(var j=0;j<labelNameMod.length;j++){
            var k =labelNameMod[j].split("级");
            if(j==labelNameMod.length-1){
                labelNameList+=dataBrr[k[0]+"级,"+k[1] ];
            }else{
                labelNameList+=dataBrr[k[0]+"级,"+k[1] ]+",";
            }

        }
        if($.trim(teachName)==""){
            addTeaInfoModel("请输入老师名字")
            //alert("请输入老师名字");
            return;
        }else if($.trim(teachPhone)==""){
            addTeaInfoModel("请输入老师手机号码")
           // alert("请输入老师手机号码");
            return;
        }else if($.trim(labelNameList)==""){
            addTeaInfoModel("请选择关联班级")
            //alert("请选择关联班级");
            return;
        }
        var data2 = {
            "teacher_name":teachName,
            "teacher_phone":teachPhone,
            "class_list":labelNameList
        }
        console.log("data2",data2)
        $.ajax({
            type:"post",
            data:data2,
            url: getURL()+"mod_teacher",
            success:function(data){
                if(data.header.code=="200"){
                    $("#accountMod").css("display","none");
                    $("#popId").hide();
                    setInterval(function(){
                        window.location.href="systemmanage.html";
                    },100)

                }
                else{
                    new Notice(dataRes.header.ms)
                    //alert(dataRes.header.msg)
                }
            }

        })
    })
    //将class和grade放到列表里面

}

//设置年级
function setGradeMod(dataGrade){
    var gradeHtml = ""
    //click-active
    for (var k = 0; k < dataGrade.length; k++) {
        gradeHtml += '<li class="" id="'+k+'grade"><span class="">' + dataGrade[k] + '</span></li>'
    }
    $("#allgradeMod").html(gradeHtml);
    $($("#allgradeMod").find("li")[0]).attr("class","click-active");

}
//设置班级
function setClassMod(dataClass){
    //默认初始化班级
    var classHtml=""
    for (var i = 0; i < dataClass[0].length; i++) {
        classHtml += '<li>' + dataClass[0][i] + ' <span class="checkbox-ico-uncheck"></span></li>'
    }
    $("#fallowClassMod").html(classHtml);
}
var indexNum=0;
function choiceGradeMod(){
    var gradeLen = $("#allgradeMod").find("li").length;
    for(var i=0;i<gradeLen;i++){
        gradeIndex.push(i+"index");
    }

    choiceClassMod(indexNum);
    $("#allgradeMod").find("li").each(function(index){

        return function(){
            $( $("#allgradeMod").find("li")[index]).on("click",function(){
                indexNum=index;
                var classData = classNameModelTep[index];
                var classHtml=""
                for(var i=0;i<classData.length;i++){
                    classHtml += '<li>' + classData[i] + ' <span class="checkbox-ico-uncheck"></span></li>'
                }
                $("#fallowClassMod").html(classHtml);
                $("#allgradeMod").find("li").attr("class","");
                $($("#allgradeMod").find("li")[index]).attr("class","click-active");
                choiceClassMod(indexNum);

                $("#connectClassMod").find("label").each(function(classNum){

                    return function(){
                        if( $($("#connectClassMod").find("label")[classNum]).text== ($("#allgradeMod").find("li")[index])){

                        }
                    }(classNum)
                })

            })
        }(index);
    })

}

function choiceClassMod(indexNum){
    findClassNameMod()

    $("#fallowClassMod").find("li").find("span").each(function(index1){
        return function(){
            $($("#fallowClassMod").find("li").find("span")[index1]).on("click",function(){

                if($($("#fallowClassMod").find("li").find("span")[index1]).hasClass("checkbox-ico-uncheck")){
                    $($("#fallowClassMod").find("li").find("span")[index1]).attr("class","checkbox-ico");
                    var htmlConnnectNum = ""
                    htmlConnnectNum += '<label class="class-item">' + gradeNameModelTep[indexNum] + classNameModelTep[indexNum][index1] + '<span></span></label>'

                    var htmlCon = $("#connectClassMod").html();
                    var allHtml = htmlCon+htmlConnnectNum;
                    $("#connectClassMod").html(allHtml)

                }else{
                    $($("#fallowClassMod").find("li").find("span")[index1]).attr("class","checkbox-ico-uncheck");
                    $("#connectClassMod").find("label").each(function(index3,value){
                        return function(){
                            if($($("#connectClassMod").find("label")[index3]).text()==(gradeNameModelTep[indexNum] + classNameModelTep[indexNum][index1])){
                                $($("#connectClassMod").find("label")[index3]).remove();
                                //实时跟新删除的代码
                                htmlConnect = $("#connectClassMod").html()

                            }
                        }(index3)
                    })

                }
                //找到对应的班级
                findClassNameMod()
                // $($("#fallowClass").find("li").find("span")[index1]).attr("class","checkbox-ico")
            })
        }(index1)
    })

}
function findClassNameMod(){
    $("#connectClassMod").find("label").each(function(index3,value){
        labelNameMod[index3]=( $($("#connectClassMod").find("label")[index3]).text())
    })
    $("#allgradeMod").find("li").each(function(index) {
        for(var i=0;i<labelNameMod.length;i++){
            if($($("#allgradeMod").find("li")[index]).hasClass("click-active")){
                for(var k=0;k<$("#fallowClassMod").find("li").find("span").length;k++){
                    var className = $($("#fallowClassMod").find("li")[k]).text();
                    var gradeName = $($("#allgradeMod").find("li")[index]).text()
                    var classAndGrade = gradeName+className;
                    if($.trim(labelNameMod[i])==($.trim(classAndGrade))){
                        $($("#fallowClassMod").find("li").find("span")[k]).attr("class","checkbox-ico");
                    }
                }

            }
        }

    })
    //每次情况
    labelNameMod=[];
}
//请求修改

//班级关联
function classAndGradeInfo(data){
    var arrGradeAndClass = new Array();
    var commonData = dataArr;
    var temp = ""
    for (var i = 0; i < data.length; i++) {
        var gradeName = commonData[data[i]].split(",")[0];
        var className = commonData[data[i]].split(",")[1];
        if (temp != gradeName) {
            gradeNameModelTep.push(gradeName);
            temp = gradeName;

        }
    }
   gradeSort(gradeNameModelTep)
    for (var k = 0; k < gradeNameModelTep.length; k++) {
        classNameModelTep[k] = new Array();
        for (var j = 0; j < data.length; j++) {
            if (gradeNameModelTep[k] == commonData[data[j]].split(",")[0]) {

                classNameModelTep[k].push(commonData[data[j]].split(",")[1])
            }
        }
        gradeSort(classNameModelTep[k])
    }
}
//发送短信通知对方
TeacherManage.prototype.createAccount = function(){
    $("#addTeacherId").on("click",function(){
        if($("#connectClass").find("label").length<=0){
            addTeaInfo("请选择关联班级")
            //alert("");
        }else{
            var teachName = $($("#accountAlter").find("ul li")[0]).find("input").val();
            var teachPhone = $($("#accountAlter").find("ul li")[1]).find("input").val();
            var schoolAccount = localStorage.getItem("schoolId");
            var labelName = new Array();
             $("#connectClass").find("label").each(function(index){
                 labelName[index]=( $($("#connectClass").find("label")[index]).text())
            })
            var labelNameList = "";
            for(var j=0;j<labelName.length;j++){
                var k =labelName[j].split("级");
                if(j==labelName.length-1){
                    labelNameList+=dataBrr[k[0]+"级,"+k[1] ];
                }else{
                    labelNameList+=dataBrr[k[0]+"级,"+k[1] ]+",";
                }

            }
            if($.trim(teachName)==""){
                addTeaInfo("请输入老师名字")
                //alert("请输入老师名字");
                return;
            }else if($.trim(teachPhone)==""){
                addTeaInfo("请输入老师手机号码")
                //alert("请输入老师手机号码");
                return;
            }else if($.trim(labelNameList)==""){
                addTeaInfo("请选择关联班级")
                //alert("请选择关联班级");
                return;
            }
            var schoolName = localStorage.getItem("schoolName")
           var data2 = {
                "teacher_name":teachName,
                "teacher_phone":teachPhone,
                "class_list":labelNameList,
                "school_id":schoolAccount,
                "school":schoolName
            }
            $.ajax({
                data: data2,
                type: "post",
                url: getURL()+"add_teacher",
                success: function(dataRes) {
                    if(dataRes.header.code=="200"){
                        $("#accountAlter").css("display","none");
                        $("#popId").hide();
                        setInterval(function(){
                           window.location.href="systemmanage.html";
                        },100)

                    }
                    else{
                        new Notice(dataRes.header.msg)
                       // alert(dataRes.header.msg)
                    }
                },error:function(){
                    new Notice("添加失败")
                   // alert("失败")
                }
            })
        }
    })

}

$(document).ready(function(){
    //获取班级
    var school=localStorage.getItem("schoolName");
    var name = localStorage.getItem("userName")
    var is_root = localStorage.getItem("is_root")
    var dataSchoolInfo ={"name":name,"schoolName":school,"is_root":is_root};
    getStuInfo(dataSchoolInfo)
    var url = getURL() + "get_user_class";
    var data = {
        "account": school
    };
    $.ajax({
        data: data,
        type: "post",
        url: url,
        success: function (dataRes) {
            console.log("teacher:",dataRes)
            classAndGradeInfo(dataRes.data.user_class);
            var teacher = new TeacherManage();
            teacher.getTeacherAcco();
            teacher.addTeacher();
            $("#accountAlter img").on("click",function(){
                $("#addTeaInfo").hide();
                $("#accountAlter").css("display","none");
                $("#popId").hide();
            })
            teacher.createAccount();
            $("#accountMod img").on("click",function(){
                $("#addTeaInfoModel").hide();
                $("#accountMod").css("display","none");
                $("#popId").hide();
            })

        }
    })

})