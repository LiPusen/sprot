/**
 * Created by hard work on 2016/2/22.
 * author:lizhihu
 * comment:学生成绩导出
 */
var gradeNameModelScore = [];
var classNameModelScore = [];
var tableHtml = ""
var defalutClassText="";
var PersonHealth = function() {
    this.termSchool = "";
    this.yearSchool = "";
    this.gradeSchool = "";
    this.classRoomSchool = "";
    $.easing.def = "easeInBack";

}
PersonHealth.prototype.getPersonListYear = function() {

    //学年
    $("#schoolYearId").on("click", function() {
        if ($("#schoolYearListId").css("display") != "block") {
            $("#schoolYearListId").slideDown(500);
            $("#schoolYearId img").attr("src", "img/moreup_gray.png");
        } else {
            $("#schoolYearListId").slideUp(500);
            $("#schoolYearId img").attr("src", "img/moredown_gray.png");
        }
        event.stopPropagation();
    });
    $("#schoolYearListId .t_list div").each(function(index, val) {
        $("#choiceSchoolYear").html($($("#schoolYearListId .t_list div")[0]).context.innerText);
        $("#schoolYearListId .t_list div")
        return function() {
            $($("#schoolYearListId .t_list div")[index]).click(function() {
                var _this = this;
                $("#choiceSchoolYear").html($(_this).context.innerText);
                $("#schoolYearListId").slideUp(500);
            })
        }(index)

    })
    //点击现在
    $("#nowYearSchool").click(function() {
        $("#choiceSchoolYear").html($($("#schoolYearListId .t_list div")[0]).context.innerText);
        $("#schoolYearListId").slideUp(500);
        $("#schoolYearId img").attr("src", "img/moredown_gray.png");
    })
    $(document).click(function(event) {
        $("#schoolYearListId").slideUp(500);
        $("#schoolYearId img").attr("src", "img/moredown_gray.png");
    })
};
PersonHealth.prototype.getPersonListTerm = function() {
    $("#schoolTermId").on("click", function() {
        if ($("#schoolTermListId").css("display") != "block") {
            $("#schoolTermListId").slideDown(500);
            $("#schoolTermId img").attr("src", "img/moreup_gray.png");
        } else {
            $("#schoolTermListId").slideUp(500);
            $("#schoolTermId img").attr("src", "img/moredown_gray.png");
        }
        event.stopPropagation();
    })
    //选择学期
    $("#schoolTermListId .t_list").each(function(index, val) {
        $("#choiceSchoolTerm").html($($("#schoolTermListId .t_list div")[0]).context.innerText);
        return function() {
            $($("#schoolTermListId .t_list div")[index]).click(function() {
                var _this = this;
                $("#choiceSchoolTerm").html($(_this).context.innerText);

                $("#schoolTermListId").slideUp(500);
                $("#schoolTermId img").attr("src", "img/moredown_gray.png");
            })
        }(index)

    })
    $(document).click(function(event) {
        $("#schoolTermListId").slideUp(500);
        $("#schoolTermId img").attr("src", "img/moredown_gray.png");
    })
}
PersonHealth.prototype.getPersonListGrade = function() {
    $("#choiceSchoolGrade").html($($("#schoolGradeListId .t_list div")[0]).context.innerText);
    $("#schoolGradeId").on("click", function() {
        //$("#choiceClass").html("请选择")
        if ($("#schoolGradeListId").css("display") != "block") {
            $("#schoolGradeListId").slideDown(500);
            $("#schoolGradeId img").attr("src", "img/moreup.png");
        } else {
            $("#schoolGradeListId").slideUp(500);
            $("#schoolGradeId img").attr("src", "img/moredown.png");
        }
        event.stopPropagation();
    })
    $("#schoolGradeListId .t_list div").each(function(index, val) {
        return function() {
            $($("#schoolGradeListId .t_list div")[index]).click(function() {
                var _this = this;
                $("#choiceSchoolGrade").html($(_this).context.innerText);
                $("#schoolGradeListId").slideUp(500);
                $("#schoolGradeId img").attr("src", "img/moredown.png");

                for (var i = 0; i < gradeNameModel.length; i++) {
                    if ($.trim($("#choiceSchoolGrade").text()) == $.trim(gradeNameModel[i])) {
                        classIdNum = i;
                    }
                }
                var classHtml = "";
                for (var j = 0; j < classNameModel[classIdNum].length; j++) {
                    classHtml += '<div><span>' + classNameModel[classIdNum][j] + '</span></div>';
                }
                $("#schoolClassListHtmlId").html(classHtml);
                $("#choiceSchoolClass").html($($("#schoolClassListHtmlId div")[0]).text());
            })
        }(index)

    })
    $(document).click(function(event) {
        $("#schoolGradeListId").slideUp(500);
        $("#schoolGradeId img").attr("src", "img/moredown.png");
    })
}
//班级
PersonHealth.prototype.getPersonListClass = function() {

    $("#schoolClassId").on("click", function() {
        if ($("#schoolClassListId").css("display") != "block") {
            $("#schoolClassListId").slideDown(500);
            $("#schoolClassId img").attr("src", "img/moreup_gray.png");
        } else {
            $("#schoolClassListId").slideUp(500);
            $("#schoolClassId img").attr("src", "img/moredown_gray.png");
        }
        event.stopPropagation();
        $("#schoolClassListId .t_list div").each(function(index, val) {
            return function() {
                $($("#schoolClassListId .t_list div")[index]).click(function() {
                    var _this = this;
                    $("#choiceSchoolClass").html($(_this).context.innerText);
                    $("#schoolClassListId").slideUp(500);
                    $("#schoolClassId img").attr("src", "img/moredown_gray.png");
                })
            }(index)

        })

    })

    $(document).click(function(event) {
        $("#schoolClassListId").slideUp(500);
        $("#schoolClassId img").attr("src", "img/moredown_gray.png");
    })

    //选择学期

}
function classAndGradeScore(data) {
    var arrGradeAndClass = new Array();
    var commonData = dataArr;
    var temp = ""
    var gradeHtml = "";
    var classHtml = "";
    var classIdNum;
    for (var i = 0; i < data.length; i++) {
        var gradeName = commonData[data[i]].split(",")[0];
        var className = commonData[data[i]].split(",")[1];
        if (temp != gradeName) {
            gradeNameModelScore.push(gradeName);
            temp = gradeName;
        }
    }
    for (var j = 0; j < gradeNameModelScore.length; j++) {
        gradeHtml += '<div><span>' + gradeNameModelScore[j] + '</span></div>'
        $("#schoolGradeListHtmlId").html(gradeHtml);
        $("#choiceSchoolGrade").html($($("#schoolGradeListHtmlId div")[0]).text());
    }
    for (var k = 0; k < gradeNameModelScore.length; k++) {
        classNameModelScore[k] = new Array();
        for (var j = 0; j < data.length; j++) {
            if (gradeNameModelScore[k] == commonData[data[j]].split(",")[0]) {

                classNameModelScore[k].push(commonData[data[j]].split(",")[1])
            }
        }
    }

    for (var i = 0; i < gradeNameModelScore.length; i++) {
        console.log("grade::",$("#choiceSchoolGrade").text())
        if ($("#choiceSchoolGrade").text() == gradeNameModelScore[i]) {
            classIdNum = i;
        }
    }
    var classHtml = "";
    for (var j = 0; j < classNameModelScore[classIdNum].length; j++) {
        classHtml += '<div><span>' + classNameModelScore[classIdNum][j] + '</span></div>';
    }
    $("#schoolClassListHtmlId").html(classHtml);
    $("#choiceSchoolClass").html($($("#schoolClassListHtmlId div")[0]).text());
    defalutClassText = $($("#schoolClassListHtmlId div")[0]).text();
}
//表格页面编辑
function editTable(){
    // 找到 tbody中所有的td操作
    //var $tds = $("table th");
    var $tds = $("#stuScore  tr td");
    // 给$tds注册点击事件
    $tds.click(function() {
        // 获取被点击的td
        var $td = $(this);
        // 检测此td是否已经被替换了，如果被替换直接返回
        if ($td.children("input").length > 0) {
            return false;
        }

        // 获取$td中的文本内容
        var text = $td.text();

        // 创建替换的input 对象
        var $input = $("<input type='text'>").css("background-color",
            $td.css("background-color")).width($td.width());
        // 设置value值
        $input.val(text);

        // 清除td中的文本内容
        $td.html("");
        $td.append($input);

        // 处理enter事件和esc事件
        $tds.hover(function(){

        },function(){
            var value = $input.val();
            // 将td中的内容修改成获取的value值
            $td.html(value);
        })
        $input.keyup(function(event) {
            // 获取当前按下键盘的键值
            var key = event.which;
            // 处理回车的情况
            if (key == 13) {
                // 获取当当前文本框中的内容
                var value = $input.val();
                // 将td中的内容修改成获取的value值
                $td.html(value);
            } else if (key == 27) {
                // 将td中的内容还原
                $td.html(text);
            }
        });
    });
}
//成绩报表导出
var idTmr;
function  getExplorer() {
    var explorer = window.navigator.userAgent ;
    //ie
    if (explorer.indexOf("MSIE") >= 0) {
        return 'ie';
    }
    //firefox
    else if (explorer.indexOf("Firefox") >= 0) {
        return 'Firefox';
    }
    //Chrome
    else if(explorer.indexOf("Chrome") >= 0){
        return 'Chrome';
    }
    //Opera
    else if(explorer.indexOf("Opera") >= 0){
        return 'Opera';
    }
    //Safari
    else if(explorer.indexOf("Safari") >= 0){
        return 'Safari';
    }
}

function method1(DivID){
    if(getExplorer()=='ie'){
        exportExcel(DivID)
    }else{
        tableToExcel(DivID)
    }
}
var tableToExcel = (function() {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) },
        format = function(s, c) {
            return s.replace(/{(\w+)}/g,
                function(m, p) { return c[p]; }) }
    return function(table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
        window.location.href = uri + base64(format(template, ctx))
    }
})()
//导出excel
function exportExcel(DivID){
    var idTmr;
    var curTbl = document.getElementById(DivID);

    var oXL = new ActiveXObject("Excel.Application"); //创建AX对象excel

    var oWB = oXL.Workbooks.Add(); //获取workbook对象

    var oSheet = oWB.ActiveSheet; //激活当前sheet

    var Lenr = curTbl.rows.length; //取得表格行数

    for (i = 0; i < Lenr; i++) {

        var Lenc = curTbl.rows(i).cells.length; //取得每行的列数

        for (j = 0; j < Lenc; j++) {

            oSheet.Cells(i + 1, j + 1).value = curTbl.rows(i).cells(j).innerText; //赋值

        }

    }

    oXL.Visible = true; //设置excel可见属性


    try {
        var fname = oXL.Application.GetSaveAsFilename("成绩导出表.xls", "Excel Spreadsheets (*.xls),*xlsx");
    } catch (e) {
        print("Nested catch caught " + e);
    } finally {
        oWB.SaveAs(fname);

        oWB.Close(savechanges = false);
        //xls.visible = false;
        oXL.Quit();

        //结束excel进程，退出完成
        //window.setInterval("Cleanup();",1);
        window.clipboardData.setData('text','');
        oXL = null;//释放对像
        oWB = null;//释放对像
        oWB = null;//释放对像
        idTmr = window.setInterval("Cleanup();", 1);

    }
}
function Cleanup() {
    window.clearInterval(idTmr);
    CollectGarbage();
}
function getDefault(){
    this.termSchool = $.trim($("#choiceSchoolTerm").text());
    this.yearSchool = $.trim($("#choiceSchoolYear").text());
    this.gradeSchool = $.trim($("#choiceSchoolGrade").text());
    this.classRoomSchool = $.trim($("#choiceSchoolClass").text());
    var schoolClassTerm = this.termSchool;
    schoolClassTerm = dataTerm[schoolClassTerm];
    $("#titleName").html( this.yearSchool+"年" + this.termSchool + this.gradeSchool+ classRoomSchool+"成绩信息");
    var class_id=dataBrr[$.trim(this.gradeSchool)+","+$.trim(this.classRoomSchool)];
    var url = getURL()+"score_output";
    var data = {"class_id":class_id,"school_id":"1","year":this.yearSchool,"term":schoolClassTerm};
    console.log(data)
    $.ajax({
        data: data,
        type: "post",
        url: url,
        success: function(dataRes) {
            if(dataRes.header.code=="200"){
                getScoreInfo(dataRes.data.report_list);
                editTable();
                $("#scoueOut").on("click",function(){
                    method1("stuScore");
                })
            }else{
                alert(dataRes.header.msg)
            }

            //获取值
        }
    });
}
function getAllData(){
  var _this = this;
    $("#schoolFindStudentInfo").on("click",function(){
        getDefault();
    })
}
function getScoreInfo(data){
   var tableHtml = "";
    if(data.length<1){

        tableHtml="";
        $("#noSchoolData").show();
    }else{
        $("#noSchoolData").hide();
        tableHtml+='<tr>';
        for(var k=0;k<data[0].length;k++){
            tableHtml+='<th>'+data[0][k]+'</th>'
        }
        tableHtml+='</tr>';
        for(var i=1;i<data.length;i++){
            tableHtml+='<tr>';
            for(var j=0;j<data[i].length;j++){
                if(j==6){
                    var sex = sexChange[data[i][j]];
                    tableHtml+='<td>'+sex+'</td>';
                }else{
                    tableHtml+='<td>'+data[i][j]+'</td>';
                }

            }
            tableHtml+='</tr>'
        }

    }
    $("#stuScore").html(tableHtml);
}
$(document).ready(function(){
    var school=localStorage.getItem("schoolName");
    var name = localStorage.getItem("userName");
    var is_root = localStorage.getItem("is_root")
    var dataSchoolInfo ={"name":name,"schoolName":school,"is_root":is_root};
    var url = getURL() + "get_user_class";
    var data = {
        "account": school
    };

    getStuInfo(dataSchoolInfo)
    $.ajax({
        data: data,
        type: "post",
        url: url,
        success: function (dataRes) {
            classAndGradeScore(dataRes.data.user_class);

            var user = localStorage.getItem("user");

            $(".userName").html(localStorage.getItem("userName"));
            $(".schoolName").html(localStorage.getItem("schoolName"))
            var stuScore = new PersonHealth();
            stuScore.getPersonListYear();
            stuScore.getPersonListTerm();
            stuScore.getPersonListGrade();
            stuScore.getPersonListClass();
            getDefault();
            getAllData();

        }
    });
})